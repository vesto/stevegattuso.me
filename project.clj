(defproject vesto "1.0"
  :description "Static site generator for vesto.me"
  :url "https://vesto.me"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [hiccup "1.0.5"]
                 [garden "1.3.10"]
                 [babashka/fs "0.2.12"]
                 [ring "1.9.6"]
                 [markdown-clj "1.11.4"]
                 [timofreiberg/bultitude "0.3.1"]
                 [clj-commons/clj-yaml "1.0.26"]
                 [image-resizer "0.1.10"]
                 [clj-http "3.12.3"]
                 [cheshire "5.11.0"]

                 ; TODO: Is this the right place to put this dev dependency like this?
                 [org.clojure/tools.trace "0.7.11"]]
  :main ^:skip-aot vesto.main
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
