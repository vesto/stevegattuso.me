# stevegattuso.me
This repository contains everything necessary to generate the content I publish on [stevegattuso.me](https://www.stevegattuso.me). It uses a custom-built static site generator, written in Clojure.

I'm not quite ready to talk about the site generator in detail, but I've published an initial introduction on [my blog](https://www.stevegattuso.me/2023/02/03/new-site-generator.html) for those who are interested in learning more. You can also poke around yourself by looking in the `src/jupi` directory, which contains all of the namespaces for the static site generator that, at some point, I'm hoping to release as a library.
