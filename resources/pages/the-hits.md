---
nav:
- About
- Just Give Me The Hits
title: Just Give Me The Hits
created-at: 2023-02-25
updated-at: 2023-02-25
wiki-id: the-hits
---

A list of some of the better blog entries and pages worth visiting on my site, affectionately named after _[Shut Up And Play The Hits](https://en.wikipedia.org/wiki/Shut_Up_and_Play_the_Hits)_.

### Tech
I like writing how-tos and guides for various technical projects of mine. These are some of the better/more popular ones I've written over the years.

* [[tech-stack-finances-budgeting]] - My absurdly overcomplicated budgeting setup.
* [[gps-to-timezone-postgis]]
* [[postgis-reverse-geocoder]]

### Art
I'm working on being more open about my creative side. For now this is a shorter list than I'd like it to be.

* [[photos-south-america]] - My favorite collection of photos taken on a point-n-shoot during a trip to South America right before the pandemic.

### New York Data Stuff
I briefly attended NYU for a masters in Urban Planning and had to write up some reports for a data analysis/visualization class. The projects were pretty fun and yielded some interesting posts:

* [[citibike-topography]]
* [[new-york-dirty-electricity-peaks]]
* [[open-restaurants-numbers]]
