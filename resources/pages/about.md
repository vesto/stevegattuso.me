---
nav:
- About
wiki-id: about
emit-path: about/index.html
title: About
created-at: 2023-02-09
updated-at: 2023-02-09
---

<center>
    <img style="border-radius: 175px" src="/static/images/me.jpg" width="250" />
</center>

I'm a New Yorker currently living in southern Germany. By trade I'm a programmer who is obsessed with the web, and as such a lot of my work ends up compiled down to some kind of HTML, CSS, JavaScript, or at least some plaintext wrapped in TCP packets.

Outside of programming I tend to bounce around between a variety of hobbies. I love traveling (especially by train), learning about urban planning, cooking vegan meals, cycling, and sailing.

## This site
Everything you see here is generated using my own custom built static site generator, Jupi, which is written in Clojure. The website is open source and can be [found on Codeberg](https://codeberg.org/vesto/stevegattuso.me). I've also written a bit about some of the neat aspects of Jupi here: [[new-site-generator]].

I use this space as an always-in-progress place to document my ideas, projects, ramblings, and anything else I deem worthy of sharing with the world.
