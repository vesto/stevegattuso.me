---
nav:
- Wiki
- Tech Stack
wiki-id: tech-stack-index
title: Tech Stack
created-at: 2022-07-17
updated-at: 2022-11-10
---

## Sections

_I'm currently publishing these sections piecemeal as I have time. Unlinked sections are ones that I've made a lot of progress on and will be writing up Soon&trade;._

## Foreward
I've spent a large amount of time slowly but surely weening myself away from the products and sleuthing eyes of large tech companies. I've found that, given a bit of up-front effort in research and setup, it is possible to keep many of the conveniences that modern tech companies offer while still retaining ownership over and privacy within your digital life. I have a preference for using open source and/or self-hosted solutions, but in the interest of being practical I am also perfectly fine with paying for hosted services where appropriate (and always to smaller companies whose business practices I find compatible with my ethical beliefs[^1]).

The pages you see here are my attempt at documenting the setup that I've landed on over the years, alongside the successes and failures that lead me to where I am now. My hope is that this documentation will inspire and guide others towards a digital life far-flung from the tentacles of the modern tech giants. I want people to know that it's both possible _and practical_ to do so!

Should you choose to begin a path of migrating away from large tech companies, your path will likely differ from mine. Different people live different lives that may be more or less conducive to being "off-the-grid." All this to say, your mileage may vary and I encourage you to pick, choose, and adapt anything you see here to your own personal preferences and needs. Consider documenting these adaptations along the way (and let me know so I can link to your findings within this guide!).

Also important to note is that these pages are and always will be a work in progress. Technologies, public policy, threat models, and people change over time, and I hope to keep this updated as I alter course and learn new things. I tend to announce major changes to my writing on [Mastodon](https://hackny.social/@steve).

[^1]: I recently found [this website](https://microfounder.com/alternatives) which lists small-company alternatives to VC-backed startups' products. I can't fully vouch for its quality yet, but want to make a note of it here because it seems like an interesting resource.
