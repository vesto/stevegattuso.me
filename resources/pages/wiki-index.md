---
nav:
- Wiki
wiki-id: wiki-index
title: Wiki
cover-image: /static/photo-galleries/2020-02-south-america/thumbs/27180007.jpeg
created-at: 2023-01-25
updated-at: 2023-03-02
---

This is a place where you can catch a small glimpse into my thoughts, ideas, projects, references, writing, or really anything else.

It's a coffee table covered in papers and books that are all open for you to browse through. Go poke around! I hope you find something interesting.

## Favorites lists
* [[books]] - Books I've read, enjoyed, and would recommend.
* [[links]] - Interesting places I've found around the internet.

## Tech Stack
The various products, services, and projects I use to power my digital life and become more illegible to prying eyes. I wrote a forward about my motivations for publishing these here: [[tech-stack-intro]].

* [[tech-stack-email]]
* [[self-host-rss]]
* [[tech-stack-photo-storage]]
* [[tech-stack-docker]]
* [[tech-stack-file-storage]]
* [[tech-stack-finances-budgeting]]

## Projects
* [[sonos-vinyl]] - A how-to for connecting a record player to a Sonos speaker system.
* [[obsidian-bookshelf]] - A dynamic bookshelf page for Obsidian (dataview).
