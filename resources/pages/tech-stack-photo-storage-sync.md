---
nav:
- Wiki
- Tech Stack
- Photo Storage and Sync
title: Photo Storage and Sync
wiki-id: tech-stack-photo-storage
created-at: 2022-08-08
updated-at: 2022-08-08
---
_Replacing iCloud and Google Photos with a self-hosted equivalent._

## Why does this matter?
* It goes without saying that your photo library can include many personal details about you that you may not want anybody or everybody to see.
* Sensitive photos are an obvious reason to be protective of this data, but there are more subtle details about your life that can surface through your photo library:
	* Who you associated with (via facial recognition)
	* Where you've been over time (via GPS data stored in EXIF metadata), places you frequent.
	* Information about your habits and interests. If you have many photos in many different locations around the world it's likely that you're an avid traveler. This is useful information to advertisers!
* If Google or Apple reserve the right to arbitrarily terminate your account. If this happens you risk losing your photo library, assuming you don't back your photos/videos up to another location.

## Storing and Viewing Photos
Given the above risks, I decided self-hosting my photo library was the best way to keep my data safe. My research into the various photo library software available lead me to [PhotoPrism](https://photoprism.app/). It's an open source, web-based photo manager that had some nifty features I couldn't find in other open source applications:

* Facial recognition to tag friends/family in photos
* A map view to see where I've taken photos
* Image recognition to tag photos (ie you can search for photos with cats or trains in them)
* An interface that looks good and works well on both desktop and mobile browsers
* Support for a variety of photo/video formats, including iOS' live photos. 

Installation is also relatively simple and can be done using a [docker-compose](https://docs.photoprism.app/getting-started/docker-compose/) file.[^1]

If you decide to go down the path of self-hosting your photo library keep in mind that it is enormously important to maintain an off-site backup of your library. The permanence of your data is decided entirely by you. You should anticipate your PhotoPrism server dying at some point and have a backup/recovery plan in place, otherwise you may find yourself without a photo library![^2]

## Syncing Photos
With PhotoPrism up and running, the next question was how to conveniently get photos synced from my phone. [PhotoSync](https://www.photosync-app.com/home.html) was the best solution I found, as it has explicit support for syncing to PhotoPrism via WebDAV. You can set it up to automatically sync each night, uploading the photos and videos you took over the previous day and optionally deleting them off of your phone to save disk space.

[^1]: At some point in the future I'll write up documentation for how I handle the nitty gritty of getting a docker-based self-hosted server up and running.
[^2]: My advice for this will come shortly in the form of a page on my strategy for file syncing and backup. All of my self-hosted services rely on this strategy to ensure data is safe.
