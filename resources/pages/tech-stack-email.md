---
nav:
- Wiki
- Tech Stack
- Email
wiki-id: tech-stack-email
title: Email
created-at: 2022-07-17
updated-at: 2022-07-17
---
_Considerations for maintaining privacy and autonomy over your email._

## Why does this matter?
There are a few reasons you should care about where you host your email:

* Most of your online identities are backed by your email address. Think of how you register for and sign into most of the services you use online. Whoever has control of your email address wields an enormous power over your online accounts.
    * Particularly scary is companies like Apple or Google arbitrarily disabling your account, which could trigger losing access to any other internet accounts linked to your email address on their service[^1].
* Your communication over email can reveal an enormous amount about your digital and physical identity. Unless you host your own email (complicated) it's _very_ important to be able to trust your email provider _and client_[^2] not to snoop on your communications.[^3]

## Picking a provider
I switched over to [Fastmail](https://www.fastmail.com) back in 2014 and have been happy with them as a provider ever since. A few things that I've really enjoyed:

* I pay for their service, meaning I can have more trust that their incentives align with mine and they won't attempt to harvest data out of my email.
* They work with my custom domain, allowing me to switch providers in the future if I become dissatisfied with their service in the future (ie preventing vendor lock-in).
* They have excellent support for IMAP/SMTP and are [proponents for open standards for email](https://fastmail.blog/tag/open-technologies/).
* They've been around since 1999. A longer history makes me feel like I can rely on them.

A major downside of Fastmail to be aware of is that it is based out of Australia, a member of the [Five Eyes](https://en.wikipedia.org/wiki/Five_Eyes) alliance and a host to some pretty [draconian data privacy laws](https://en.wikipedia.org/wiki/Mass_surveillance_in_Australia#Telecommunications_and_Other_Legislation_Amendment_(Assistance_and_Access)_Act). Depending on your threat model, this may or may not be a dealbreaker. [Proton Mail](https://proton.me/mail) is a frequently-mentioned alternative that may be worth investigating. I haven't tried them out yet because I've been happy with Fastmail, but if I were to start my search anew they would be top of my list of candidates.

An important feature to keep an eye out for when investigating providers is the ability to create one-off email addresses. Fastmail, for example, allows me to create email addresses like `something@steve.stevegattuso.me`. This allows me to create a unique email address for every service, making it a little bit harder for companies who sell my data to link my identity between services[^4]. They recently released [masked email](https://www.fastmail.help/hc/en-us/articles/4406536368911-Masked-Email) which takes this a step further by allowing users to generate more anonymous email addresses and easily disable/block them. This is a great feature, but keep in mind it can be a form of vendor lock-in, as these generated email addresses are not as portable as a domain-specific email address like `something@yourdomain.com`.

## Why not self host?
If you feel technically capable of doing so, you can and should do so! I've chosen not to go down this path due to the relative complexity involved in setting up and properly maintaining the suite of software required for a functional/modern email stack. If you're brave and want to go down this path, [docker-mailserver](https://github.com/docker-mailserver/docker-mailserver) looks to be an interesting project to start your investigation with.

[^1]: For this reason I'd also recommend using an email address on your own custom domain name + _never_ using a "Sign In With X" authentication option. Always sign up for accounts using your email address unless there is no other option.
[^2]: See [this post](https://daringfireball.net/linked/2020/06/01/product-info-in-amazon-emails) from John Gruber discussing some of the privacy dangers that can come arise from a problematic email provider _and client_.
[^3]: Note that, since 2017, [Google claims](https://www.nytimes.com/2017/06/23/technology/gmail-ads.html) that they do not use the contents of your email to target ads. This sounds great, however there is nothing preventing them from changing this policy in the future.
[^4]: This is a form of practicing [digital illegibility]({% post_url 2022-02-21-illegibility-digital-lives %})!
