---
nav:
- Wiki
- Tech Stack
- File Syncing
wiki-id: tech-stack-file-storage
title: File Syncing
created-at: 2022-08-25
updated-at: 2022-08-25
---

_A capable self-hosted and FOSS replacement for Dropbox, Google Drive, etc._

After bouncing around various paid hosting services[^1], I finally settled on using [Syncthing](https://syncthing.net/) as my software of choice for keeping directories in sync across my devices. A few reasons for the selection:

* Syncthing is free and open source software. It's been around for a while and has reached a relatively mature state. This means the UI does not arbitrarily change for a constantly expanding set of features (this was a very annoying behavior of paid providers).
* Syncthing synchronizes files peer-to-peer amongst your devices; it does not need a centralized node to coordinate the service. However, it does benefit from having an always-on node that all devices can sync with if others are offline.
* The only storage limit is the amount of disk space on your devices.
* All data is transferred over an encrypted connection- no need to worry about trusting a third party.
* It is possible to set up untrusted nodes that only store an encrypted copies of your files. This means you can use a relatively untrusted cloud provider as an always-on node and still be sure that your files cannot be accessed.[^2]

This all sounds great, however the one major downside to Syncthing is its lack of mobile application. This kept me from using it for quite a while until I finally found a workaround (at least for iOS). Enter [Secure Shellfish](https://secureshellfish.app/)[^3]. While it looks like a run-of-the-mill SSH client for iOS, it has a nifty feature of allowing you to mount a remote device onto iOS's Files app. If you have an always-on server running Syncthing you can use this as your mount device to get mobile access to your files. iOS' Files app has all of the normal filesystem features you'd expect: uploading, downloading, scanning documents in, etc. The experiences ends up being pretty seamless.

I don't have a similar recommendation for Android users just yet, but I'm sure something similar exists. If you're reading this and end up giving it a shot, send me an email and let me know what has worked for you so I can post it up here.

While the redundancy of having my filesystem in sync on multiple devices is pretty safe for ensuring no data gets lost, I also fall back to [BorgBackup](https://www.borgbackup.org/) to ensure I have an entirely separate off-site backup. You can read more about this setup [here](https://www.stevegattuso.me/tech-stack/hosting-services.html) (ctrl+f for the borg section).

[^1]: The one I stuck with the longest and recommend if you're unwilling to go down the Syncthing path is [Tresorit](https://tresorit.com/). Note that I selected this service quite a while ago and there may be companies offering a more secure solution these days. My biggest gripe with them was that their client software was all closed source.
[^2]: I have not tested this personally and cannot fully vouch for it being 100% safe/leak free.
[^3]: The app is paid but I have no affiliation with the developer.
