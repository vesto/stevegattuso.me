---
title: Obsidian bookshelf
nav: ['Projects', 'Obsidian Bookshelf']
emit-path: projects/obsidian-bookshelf/index.html
wiki-id: obsidian-bookshelf
collection: projects
cover: obsidian-bookshelf.png
description: A dynamic bookshelf page for Obsidian.
created-at: 2022-02-02
updated-at: 2022-02-02
---
I like keeping a record of the books I've read over the years in addition to some notes and thoughts I gather while reading them. Since adopting [Obsidian](https://obsidian.md/) and Zettelkasten I've been searching for the best way to organize these records and notes into a unified index page. This index page should show a list of books I've read in chronological order, noting their current status (read, reading, todo, ditched) in addition to showing when I started/finished them.

Given that Obsidian is largely a repository of static Markdown files, creating such an index seemed like a daunting task. In the past I've written python scripts that find notes labeled as "books", extract some metadata, and then output a markdown index page. These work, but it’s not quite ideal because it requires running the script each time you edit a file's contents.

Enter the [Obsidian Dataview plugin](https://blacksmithgu.github.io/obsidian-dataview/). This addon allows for generating index pages based on Markdown files (and their frontmatter) within your vault. Even better, it exposes a Javascript API that allows for some really in-depth customization of how these index pages are displayed.

After quite a bit of tinkering, I ended up with this:

![A screenshot of the bookmark](/static/projects/obsidian-bookshelf.png)

You can find the source code which generates this index [here](https://paste.sr.ht/~vesto/64597ed56d6493673036fa498db2c0b74f81f728). Remember that you'll need to wrap the code in a [dataview block](https://blacksmithgu.github.io/obsidian-dataview/api/intro/) in order for it to get picked up.

Each of the pages this index links to begin with some yaml frontmatter in order to assign the metadata:

```
---
alias: The Dispossessed
author: Ursula K. Ke Guin
status: read
started: 2022-01-01
completed: 2022-01-15
tags:
- books
- fiction
- political
---
```

The most important of this metadata is the books tag, which allows the Dataview to find the markdown file even though I store it as a standard Zettelkasten note. I try to keep my Obsidian vault entirely flat, that is, there are no folder hierarchies to be found. Everything is organized via links between notes and occasionally tags. This is a pretty standard practice from Zettelkasten which you can learn more about [here](https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125).
