---
date: 2020-06-21
title: Redesigned blog
slug: new-website
categories: microblog
---
As it turns out, moving to a new city just before a pandemic hits is a great
way to find yourself with plenty of free time. I finally got bored enough to
redesign my personal website and hook it up to [micro.blog](https://micro.blog/stevenleeg),
which will ideally auto-syndicate posts to [Twitter](https://twitter.com/stevenleeg), [Mastodon](https://mastodon.technology/stevenleeg), and [RSS](https://stevegattuso.me/feed.xml) (honestly this post is mostly just testing the xposting out).

I'm trying to be a bit better about writing these days and hope I can
publish thoughts more frequently here. For some reason it's always easy for me
to write sub-240 character Tweets and blast them off, but when it comes to this
blog I find myself frequently second-guessing and overcorrecting my writing.
This inevitably kills off the original motivation I had to write the post in
the first place (see [gumption traps](https://en.wikipedia.org/wiki/Gumption_trap)),
resulting in never hitting the publish button.

I'm thinking about writing a utility which lets me blast off blog posts directly
from [Drafts](https://getdrafts.com/), which I've been using a lot for journaling
lately. This would essentially replicate the low-friction fire-and-forget of
Twitter's compose view, but I'm still deciding if that's a desireable trait or
not.

Anyways, hope you enjoy reading with the new layout.
