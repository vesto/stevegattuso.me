---
title: A Letter from Nicole Krauss to Van Gogh
link: https://www.brainpickings.org/2015/11/09/nicole-krauss-van-gogh-letter/
slug: a-letter-to-van-gogh
preview_image: /media/saturn.jpg
date: 2020-06-22T00:55:45.872Z
categories: microblog
layout: post
---
I don't remember exactly how this quote made it into my bookmarks, but I really liked it and thought it'd be worth sharing:

> It’s a strange thing about the human mind that, despite its capacity and its abundant freedom, its default is to function in a repeating pattern. It watches the moon and the planets, the days and seasons, the cycle of life and death all going around in an endless loop, and unconsciously, believing itself to be nature, the mind echoes these cycles. Its thoughts go in loops, repeating patterns established so long ago we often can’t remember their origin, or why they ever made sense to us. And even when these loops fail over and over again to bring us to a desirable place, even while they entrap us, and make us feel anciently tired of ourselves, and we sense that sticking to their well-worn path means we’ll miss contact with the truth every single time, we still find it nearly impossible to resist them. We call these patterns of thought our “nature” and resign ourselves to being governed by them as if they are the result of a force outside of us, the way that the seas are governed — rather absurdly, when one thinks about it — by a distant and otherwise irrelevant moon.
>
> And yet it is unquestionably within our power to break the loop; to “violate” what presents itself as our nature by choosing to think — and to see, and act — in a different way. It may require enormous effort and focus. And yet for the most part it isn’t laziness that stops us from breaking these loops, it’s fear. In a sense, one could say that fear is the otherwise irrelevant moon that we allow to govern the far larger nature of our minds.

I also was browsing [this blog](https://www.brainpickings.org/) and found that the author made an amazing collection of [vintage science facemasks](https://www.brainpickings.org/2020/06/19/vintage-science-face-masks/). I love this aesthetic and instabought the Saturn mask:

![Saturn mask](/media/saturn.jpg "Saturn mask")
