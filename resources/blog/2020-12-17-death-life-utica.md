---
layout: post
categories:
    - longform
title: The Death and Life of Utica, NY
date: 2020-12-17
preview_image: /assets/img/utica-2.jpg
excerpt: |
    An anecdote I recently shared with some friends about my own experience
    growing up in Central New York, more specifically near Utica.  The city is
    a former boom town, achieving its heights in the late 1800s as a major
    textile manufacturing city. Some of the photos of the old city center paint
    the picture of a bustling little city...
---
Streetsblog recently posted [an article](https://usa.streetsblog.org/2020/12/15/what-secretary-pete-could-mean-for-the-us-dot/) expressing some optimism on the appointment of Pete Buttigieg as Secretary of Transportation. The article links to one tweet in which stuck out to me:

![](/assets/img/utica-1.png)

This reminded me of an anecdote I recently shared with some friends about my
own experience growing up in Central New York, more specifically near Utica.
The city is a former boom town, achieving its heights in the late 1800s as a
major textile manufacturing city. Some of the photos of the old city center
paint the picture of a bustling little city:

![](/assets/img/utica-2.jpg)
<div class="image-caption">Taken from sdguyer on <a href="https://www.hippostcard.com/listing/utica-new-yorkgenesee-street-southlarge-crowd-trolley-cars1920s-postcard/6494319">Hip Postcard</a></div>

![](/assets/img/utica-3.jpg)
<div class="image-caption">Taken from <a href="https://oneidacountyhistory.wordpress.com/2014/06/">Onedia County History</a></div>

I find the trollies to be especially beautiful, artifacts of a former city that I only know through stories told to me about my Great Grandmother riding them in her childhood.

Since these images were taken, a familiar story of American manufacturing cities' decline played out to its fullest. The Utica of the 21st century, the city I know, is an apparent husk of its former self:

![](/assets/img/utica-4.png)
<div class="image-caption">A view of Genesee St. on Google Maps from Aug 2019.</div>

Many of my relatives still living upstate are conservative, or at least right leaning. I've been told repeatedly by them about how great Syracuse, Utica or Rochester used to be back when they had their respective manufacturing booms. Nobody has come out and said it, but I have a hunch that this is what's in their mind when they say "make America great again."

Trump was the politician promising to bring back manufacturing, something that was the lifeblood of the cities they grew up in and around in their youth. I can see how such a promise is so luring to them, even though it predictably resulted in nothing after four years in office.

I can also see how they've been lead into the idea of enacting revenge on some fictionalized ideal of "coastal elites." Globalization and neoliberalism took so much wealth from these cities and aggregated it further into already successful places like New York, San Francisco, Boston, etc. That's frustrating, maddening; something that I'd probably want revenge for as well had I not abandoned ship myself and moved downstate to New York City. 

Regardless, whenever I'm back home I always wonder what actual success could look like for cities like Utica. I think it's become apparent that manufacturing is never going to come back to rust belt cities like these, so what next? What kind of 21st century economic engine could be created and leveraged to rebuild and revitalize a small snowy city like Utica?

It appears some students and faculty at Cornell are researching solutions to this via the [Rust 2 Green](https://www.rust2greenutica.org/) program. From browsing around their website it appears they've been successful in a hodgepodge of community outreach programs and released quite a few revitalization ideas as the result of student capstone courses. While I applaud the effort, I believe what a place like Utica primarily needs is an economic reason for people to be there. Some sort of industry or specialty that can put it on the map, as textile manufacturing once did. Local politicians try to shift the blame [onto state policy](https://www.uticaod.com/article/20090401/NEWS/304019918) as a reason for being ranked so low for economic opportunity, but it reeks of people still seeking out a revival of manufacturing rather than trying to create something new.

I don't have a good answer for places like Utica. It's a topic I want to study more, but for the time being I wanted to get this idea stuck in my head out into the wild for others to think about as well. With a new federal administration coming into office I think a window of opportunity to make progress is once again opening in the U.S. I hope that we can avoid squandering it by at least trying to figure out how to bring life back to cities outside of the typical superstars that have prospered in the last two decades. 
