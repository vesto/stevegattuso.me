---
title: Getting off of social media
categories: microblog
date: 2020-07-20T12:22:45.227Z
---
I've been progressively trying to detach myself from social media and am finally calling it quits on the last network I checked with any sort of regularity: Twitter. I just don't think anything good comes from scrolling through an endless feed of bad news, echo chambers, and bottom-of-the-barrel hot takes. Or worse, [doomscrolling](https://www.npr.org/2020/07/19/892728595/your-doomscrolling-breeds-anxiety-here-s-how-to-stop-the-cycle)– which is a phenomenal term. It just feels like you're binging on the brain-equivilent of doritos. I'll still keep my account around to syndicate content, but this is done automatically at this point so I don't actually need to log in (and subsequently be tempted to start scrolling).

Instead I've been subscribing to interesting content via RSS (using [Feedly](https://feedly.com/)). I find this to be much less stressful, as there's a finality to the feed of content that doesn't exist on social media platforms. Once you've browsed through all of the new items in your feeds, you're done. There's nothing more to look at.

If you're a hacker news user, I'd also recommend replacing the front page with one of the feeds at [hnrss](https://edavis.github.io/hnrss/). I've subscribed to one that surfaces any post with >200 points, filtering out a lot of the noise that would previously consume my time. [1Blocker](https://1blocker.com/) on iOS or [Impulse Blocker ](https://addons.mozilla.org/en-US/firefox/addon/impulse-blocker/)on Firefox are also a huge help in ensuring you're not subconsciencely navigating to social media websites to take a hit of content dopamine.

If you have any other tips/strategies for taming the internet, [drop me a line](mailto:steve@stevegattuso.me).