---
layout: post
categories: frontpage microblog
title: "Indie Microblogging: owning your short-form writing"
link: https://www.kickstarter.com/projects/manton/indie-microblogging-owning-your-short-form-writing
link-domain: kickstarter.com
date: 2017-01-11
---
I like the idea of moving my Facebook/Twitter posts onto a medium that I 
actually control, even if that's at the expense of some viewership.

[Micro.blog](http://micro.blog) seems like an interesting idea that's worth supporting if you're
interested in a more decentralized web.
