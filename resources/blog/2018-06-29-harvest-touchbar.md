---
layout: post
categories: frontpage microblog
title: Harvest for Touchbar
link: https://github.com/stevenleeg/harvest-touchbar
link-domain: github.com
date: 2018-06-29
---

I finally found myself with a perfect combination of free time and a useful
idea and managed to put together a small widget for
[BetterTouchTool](https://folivora.ai/) which duplicates the menu bar UI for
[Harvest for Mac](https://www.getharvest.com/apps/mac). If you use Harvest to
keep track of your time, [check it out](https://github.com/stevenleeg/harvest-touchbar)!
