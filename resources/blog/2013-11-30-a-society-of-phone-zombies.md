---
layout: post
title: A society of phone zombies
summary: Put your damn phone away.
categories:
- longform
- best-of
category: frontpage
date: 2013-11-30
slug: a-society-of-phone-zombies
---
I tell a lot of people that I'm a computer hacker. With this sort of
identity, I tend to surprise a lot of people when I say that I actually hate
technology. Yes, when I say this I am exaggerating a bit, but there is truth
to what I'm saying. What I really mean is that I've come to
greatly dislike the way technology has been invading our lives.

If you've walked on any populated streets lately, I'm sure you
know what I'm talking about. Over the last 3-4 years there has been a
sharp increase in the number of people who stare at their smartphones while
walking on the street, completely oblivious of the world around them. If you
watch people with any sort of downtime (or procrastination habits),
you're bound to see them scrolling through an endless Facebook,
Twitter, or Tumblr feed. Hell, I've even seen people scrolling through
their phones while watching the TV show they've been waiting all week
to see.

Don't get me wrong, technology is a great thing that has helped us reach _amazing_ feats (reminder: we have sent humans to the fucking moon). That being said, I think some of our recent inventions with technology have actually decreased the quality of our lives rather than improving it. We've become a society of phone zombies, constantly staring down at our glowing screens in order to gobble up endless amounts of useless information.

An obvious cause is the huge growth of mobile as of late. A study from Pew Internet suggests that as much as <a href='http://www.pewinternet.org/Reports/2013/Smartphone-Ownership-2013/Findings.aspx'>56% of American adults</a> own smartphones. That's a huge number of people all suddenly having instant-on internet ready devices sitting in their pockets. This definitely sets the stage for abuse, but the core cause of our obsession stems from what we actually do on these devices.

While companies like Apple, Samsung, and HTC all compete over which device you use, there is a mass of companies competing for something much more important: your attention. Many of these companies are profiting off of advertisements, therefore, it is in their interest for you to use their applications as much as possible. Think about companies like Facebook, Twitter, or my least favorite, Buzzfeed. These are all companies whose revenues increase as their users' attention increases. The problem is that we've made it incredibly easy for them to grab and keep our attention, and these companies have taken complete advantage over it.

Think about it. There's a good chance that you have an internet-capable device sitting in your pocket (or within reach of you) at all times. This device has the ability to make sound or vibrate at any time, letting you know that something important or relevant to you has just happened. It's also instantly available, allowing you to quickly turn it on, glance at information, and turn it off whenever you may need or want it. On top of this, we have companies who are financially incentivized to have you interacting with your device as much as possible in order to get more eyes on more advertisements. Is it any wonder why we've become a society that is obsessed with our phones?

I believe that this is a problem that has caused us to care less about real life and more about our precious phones feeding us with useless information and quick entertainment. Why endure a full 30 seconds of boredom if we could pull out our phones and laugh at a quick gif? We've been trained to become a society of internet-addicts, constantly craving the entertainment provided by our phones rather than enjoying the people and experiences of our lives in the real world.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/OINa46HeWg8" frameborder="0" allowfullscreen></iframe>

<div class='image-caption'>
  Charstarlene's "I forgot my phone" does a great job of illustrating this problem.
</div>

---

I think the solution to this problem comes in two parts. The first comes from ourselves. If what I'm saying resonates with you, I'd encourage you to become more conscious about your internet and phone usage. Next time you pull your phone out, think about what caused you to take it out of your pocket. Is this reason _really_ important enough to justify interrupting whatever is going on around you?

In addition, I've been trying to encourage my friends to put their phones away whenever I'm around. A few weeks ago, while out with some friends, I took everyone's phone and powered them off for the night, saying that we were going to focus on the night rather than our stupid fucking phones. At first it annoyed everyone, as I was clearly being obnoxious about imposing my way on them. However, at the end of the night I actually had a few people say they appreciated what I did. They told me that they enjoyed the night more without having to be constantly distracted by their phones buzzing in their pockets, trying to steal their attention away from the fun 
