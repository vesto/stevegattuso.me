---
layout: post
categories: frontpage microblog
title: Librem 5 - A Security and Privacy Focused Phone
link: https://puri.sm/shop/librem-5/
link-domain: puri.sm
date: 2017-10-09
---
This campaign looks awesome, and it got funded! There have been quite a few
attempts at these types of phones and operating systems in the past (namely
Firefox OS and Ubuntu Mobile), but none have really been successful as far as
I'm aware.

We'll have to wait and see if Purism can be the exception, but I'm rooting for
them. I'd love for there to finally be an open-source option in the mobile
world, as I don't think the existing duopoly of Android and iOS is healthy.
It'll always be a niche market (just as desktop Linux is today), but I think
that's just fine. The important thing is having hardware and software built by
people who (ideally) have users' best interests in mind rather than their
shareholders'.
