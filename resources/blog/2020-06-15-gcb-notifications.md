---
layout: post
categories: microblog
title: "Better Google Cloud Build notifications in Slack"
link: https://amperon.co/blog/better-gcb-notifications-slack/
link-domain: amperon.co
date: 2020-06-15
---
If your company uses Google Cloud Build you may have noticed that their out-of-the-box Slack integration is... Lacking. I've published a much improved Slack notification on Amperon's blog for you to steal!
