---
title: A Message from Your Friendly Local Mail Carrier
categories:
- microblog
date: 2020-10-09
link: https://www.youtube.com/watch?time_continue=143&v=73pilnstgt0&feature=emb_title
preview_image: /static/media/seinfeld-newman-1.png
published: false
---

Pure excellence.
