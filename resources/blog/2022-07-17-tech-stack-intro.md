---
title: Documenting my tech stack
categories: microblog
date: 2022-07-17
summary: Documenting the various tools and services I've set up for myself in the pursuit of avoiding big tech's eye.
---

I've started to put together a new set of pages on my [[wiki-index]] where I'll be documenting the work I've been doing towards migrating away from giant tech companies in favor of self-hosted, open source, and privacy-preserving online service providers.

I've spent a large amount of time slowly but surely weening myself away from the products and sleuthing eyes of large tech companies. I've found that, given a bit of up-front effort in research and setup, it is possible to retain many of the conveniences that modern tech companies offer while still retaining ownership over and privacy within your digital life. I have a preference for using open source and/or self-hosted solutions, but in the interest of being practical I am also perfectly fine with paying for hosted services where appropriate (and always to smaller companies whose business practices I find compatible with my ethical beliefs).

These pages are my attempt at documenting the setup that I've landed on over the years, alongside the successes and failures that lead me to where I am now. My hope is that this documentation will inspire and guide others towards a digital life far-flung from the tentacles of the modern tech giants. I want people to know that it's both possible _and practical_ to do so!

Should you choose to begin a path of migrating away from large tech companies, your path will likely differ from mine. Different people live different lives that may be more or less conducive to being "off-the-grid." All this to say, your mileage may vary and I encourage you to pick, choose, and adapt anything you see here to your own personal preferences and needs. Consider documenting these adaptations along the way (and let me know so I can link to your findings within this guide!).

Also important to note is that these pages are and always will be a work in progress. Technologies, public policy, threat models, and people change over time, and I hope to keep this updated as I alter course and learn new things. I tend to announce major changes to my writing on [Mastodon](https://hackny.social/@steve) or via my [RSS feed](/feed.xml).

The first piece I've decided to document is [[tech-stack-email]]. It's a small first step towards dumping a large amount of information that I've been procrastinating on writing/sharing. Hope you find it interesting and/or useful!
