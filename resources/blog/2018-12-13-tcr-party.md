---
layout: post
categories: frontpage microblog
title: "TCR Party: A Token Curated Registry for Humans"
link: https://medium.com/alpineintel/tcr-party-a-token-curated-registry-for-humans-f01c33a7dd32
link-domain: medium.com
date: 2018-12-13
---
Lately I've been working on a research project at work called TCR Party, a
token curated list of the top Twitter handles in the crypto community, powered
by an existing human-readable interface.

And yes, I recognize the irony of publishing this article on Medium given my
[previous writing](https://www.stevegattuso.me/2018/03/01/own-your-content.html). I'm actively trying to move my team away from it 🙂.
