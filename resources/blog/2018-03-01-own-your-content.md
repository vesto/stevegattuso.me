---
layout: post
categories:
    - longform
title: The case for self-publishing content
date: 2018-03-01
preview-image: static/blog-images/own-your-content-header.jpg
summary: |
    Our best shot at combatting tech companies' power grab on the internet is
    to stop giving our content to Facebook, Twitter, Medium, etc. for free.
    Instead, let's take our content into our own hands by self-publishing on
    our own personal websites, a medium of communication which is free of
    censorship, algorithmic ranking, and increasingly intrusive advertising.
slug: own-your-content
---

I'll be honest, it's a frustrating time to be a citizen of the internet. You'll
probably notice from some of my
[recent](/2017/10/16/quit-facebook.html)
[posts](/2017/10/08/attention-economy.html) that I've become a
pretty
big advocate for moving away from the giant tech companies that have been
slowly but steadily killing the open and decentralized web that brought us to
where we are today.

I believe our best shot at combatting tech companies' power grab on the
internet is to stop giving our content to Facebook, Twitter, Medium, etc. for
free. Instead, let's take our content into our own hands by self-publishing on
our own personal websites, a medium of communication which is free of
censorship, algorithmic ranking, and increasingly intrusive advertising. Plus,
building your own website and filling it with your weird, fun, and interesting
content can be much more rewarding for yourself, your audience, and the
internet as a whole.

Before diving in, I want to emphasize that I recognize building a nice looking
website and publishing your content is something that can require a fair bit of
technical know-how in order to accomplish. For those of you in the tech world
who are reading this, I'm particularly writing this article for you – as you
likely have the ability to do this and tend to be the leading edge of tech.
It's important for us, of all people, to lead the way in resisting the reach of
tech companies and help maintain an open and decentralized web. To any others,
I'd highly encourage you to check out a free website builder like
[Weebly](https://www.weebly.com) (_disclaimer_: I was previously employed at
Weebly but have no financial incentive to link to them).

## Media platforms aren't as valuable as you may think 

I'd like to start out by picking on Medium. I've noticed many members of the
tech community publishing long-form articles on their platform so they're a
ripe target, although these arguments apply to just about any social media
outlet as well. The first question to ask is "why do people choose to publish
on Medium?" From asking around friends who use the platform, I've found a few
reasons. The first that came up was Medium providing a cleanly designed page
with which to present their writing. This is a fair argument, as we're not all
designers (and design is hard!), but let's take a look at an average Medium
article's design:

![Medium screenshot](/static/blog-images/own-your-content-medium.png)

Aside from the sticky title bar at the top _and bottom_ of the page (which I'd
argue is doing nothing but distracting readers from your content), there's
really not a ton going on here. It turns out designing a reasonable template
for your content doesn't have to be that hard. For a blog, a white background
and a dark, serif font works great. Note that Medium is a giant venture-capital
backed startup with many designers and they're doing it too! If you're
publishing photos, [just make them giant and fill up the
page](https://www.stevegattuso.me/travel/2016-spring-europe/amsterdam). It
really doesn't have to be anything fancy, as your content should be what keeps
the user engaged, not the frame around it. If designing a blog theme isn't your
thing there are also plenty of free themes that you can either use
out-of-the-box or as a starting point for your own design. There are plenty of
free templates available for [Jekyll](http://jekyllthemes.org/),
[Wordpress](https://wordpress.org/themes/), or whatever other platform you end
up with for hosting your content.

Another argument for using a platform like Medium are the various features it
provides for your content, things like embedded comments, clapping (equivilent
to liking a post on Facebook), and following. While useful to have, I'd argue
that none of these are unique to Medium itself and each can be replicated on
your own site quite easily. Comments, for example, can be delegated out to
external platforms like Twitter, Reddit or Hacker News. For many bloggers these
platforms are the source of most of their traffic anyways. Users from these
sites are generally much more familiar with discussing content there rather
than on a site's dedicated comments section. If you still find embedded
comments on your posts valuable, you can always drop an embed code for
something like [Disqus](https://disqus.com/) to handle it for you or use a
self-hosted blogging platform like Wordpress which has built-in support.

Clapping and following can be tied into the next, and by far the most
important, reason I've found for people publishing to social media platforms:
their network. If you're publishing content you're likely looking to get as
many eyes on it as possible. While social media does an excellent job of
helping you maximize viewership, publishing to your own website doesn't mean
you miss out on the visibility that platforms like Twitter, Facebook, Medium,
etc. can provide for you. It's easy: instead of posting directly to these
platforms and allowing them to bring your content to users, use them as a means
of syndication to bring users to your content. That is, after publishing
something to your blog, simply post a link to your post on Facebook, Instagram,
Twitter, Reddit, etc. to bring users to your site, rather than having them view
your content from the confines of the platform. In fact, it's likely that
you're already doing this anyways by re-posting a link to a Medium article or
Instagram photo on Twitter, so this concept shouldn't be too strange!

This might sound like a pain, but setting up a simple RSS feed (something that
a static site generator like Jekyll can do [very
easily](https://github.com/jekyll/jekyll-feed)) and using a service like
[IFTTT](https://ifttt.com/) can entirely automate this process, allowing you
to focus on creating content rather than manually promoting it.

## The benefits of self-publishing

Now that we've covered some of the ways to replicate the benefits of content
platforms on your own site, I'd like to discuss some of the benefits you'll
find from publishing content on your own. Most of these arguments revolve
around *control*. More specifically, taking control away from third-party
platforms like Facebook, Twitter, or Medium (who have enough power/control as
it is) and instead putting them into the hands of yourself and your audience.

The first of these benefits is the complete control over the presentation of
your content. While we've already touched on how media platforms provide a
nice design for your content, one aspect we didn't touch on was the drawback of
monotony. I'll again pick on Medium here: you can [pick
out](https://medium.com/@tommycm/how-a-brit-imagines-thanksgiving-39ab6e9bb47)
[any
sample](https://medium.com/hopes-and-dreams-for-our-future/when-you-feel-creatively-drained-9d4f5f34346)
[of
articles](https://medium.com/personal-growth/smartphones-harm-your-productivity-more-than-you-think-62e105655992)
featured its homepage, each of which having an entirely unique topic and
author, yet all lack any form of differentiation or individuality in their
presentation.

While this uniformity in presentation is great for building a brand, the issue
is *who* you're building a brand for. By posting an article to the uniform
template of Medium or posting an image to the uniform template of Instagram,
you're not helping build a brand for yourself, rather you're helping building
the brand of the platform (and you're doing so without them paying you for
it!). By self-publishing you're able to completely control the frame within
which your content is presented. Again, this doesn't have to be complicated in
order to be effective in building a unique brand for yourself and your content.
Check out an [average post](https://marco.org/2017/11/14/best-laptop-ever) from
a favorite of mine – Marco Arment. His design is unintrusive, simple, and most
importantly is unique and representative of himself. By delegating design out
to media platforms you miss out on a major avenue of expression/differentiation
for your content.

In addition to controling presentation, self-publishing also gives you the
ability to bring all of your content into one place rather than having it
spread off into separate walled-gardens around the internet. Take my website
for example, here you can find pretty much everything I post publically (with
the exception of my open-source code, which I'm working on self-hosting). By
going to [stevegattuso.me](https://www.stevegattuso.me) you can find articles
I've published, details about my software consulting company, galleries of my
garbage attempts at photography, a variety of travelougues, and contact
information all in one place (and with the same design!). If I were to do this
the social media way I'd be sending users from my site to Medium, Flickr,
Twitter, LinkedIn, etc. rather than having a centralized and uniform repository
for my public-facing digital presence.

The final case I have for self-publishing your content is a bit more from the
heart than any sort of utilitarian argument. The web was originally designed to
be a decentralized network of documents, linked amongst one other without any
form of centralized power to control its fate. Facebook, Twitter, Medium,
Google, Instagram, and whatever other tech company you can name are all
attempting to undermine this decentralized web in order to keep users in their
walled-gardens and better serve their real customers: advertisers. This
is not the web that we should be encouraging if we want a world controlled by
people rather than corporations. By publishing content on your own website
you're helping reject a future where content is controlled by companies.
Instead, you'll be moving us back towards an open and decentralized web whose
purpose is to be fun, interesting, and weird rather than corporate, monotonous,
and profit-seeking.

Long live the open web!

## Some fun sites I recommend
 
Before I go, here are some great examples of personal websites that I've
enjoyed paroozing through:

* [Joshua Stein](https://jcs.org/) – An OpenBSD developer with some more
  [awesome tech articles](https://jcs.org/2017/11/17/bitwarden).
* [Julia Evans](https://jvns.ca/) – A software engineer at Stripe who has some
  incredibly informative articles about various levels of
  development/computing.
* [Scott Bakal](http://www.scottbakal.com/) – I'm not sure how I found this,
  but I love his art, this website, and have been following his studio blog
  for a while now.

Also: this isn't a personal site but basically anything off of
[Neocities](https://neocities.org/) is awesome – it's a weird and amazing
creative corner of the web that I love surfing through.
