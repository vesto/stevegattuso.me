---
layout: post
title: Privacy is Power
link: https://journal.standardnotes.org/privacy-is-power-f0a064ab36ea#.adsisz6wg
link-domain: journal.standardnotes.org
date: 2017-02-07
---
I really like the way this article articulates why putting effort into digital
privacy is worth our time. Recently I've been moving myself away from cloud
services in favor of self-hosted open-source projects. I've switched from using
GitHub to [Gogs](https://gogs.io/), Evernote to [vimwiki](https://github.com/vimwiki/vimwiki), 
GitHub pages to my own web server (deployed by [Jenkins](https://jenkins.io/index.html)), 
encrypted backups using [borgbackup](https://github.com/borgbackup/borg), and
Dropbox to [Syncthing](https://syncthing.net/).

It's been a lot of fun building this self-hosted network of services, but it's
also important to recognize my privilege in being able to do so. Not everyone
has the time, motivation, and skill-set to replace the various cloud services
that may be snooping on their lives. Even more concerning is the general
population's lack of interest in protecting privacy; I all too frequently hear
friends and family use the argument "I have nothing to hide, so why should
inconvenience myself?"

I think there is a lot of untapped potential in creating open-source,
encrypted, and decentralized software that is more accessible to the masses. A
combination of privacy education and easy-to-use software could be a potent
force against government and corporate snooping. It's obviously much easier
said than done, but I think it's an important battle to continue fighting
nonetheless. 

Quick plug: the [EFF](https://www.eff.org/) is fighting this exact battle. If
you're interested in helping the cause and are financially able, please
consider donating!
