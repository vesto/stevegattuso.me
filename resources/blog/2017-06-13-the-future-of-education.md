---
layout: post
categories: frontpage microblog
title: The Future of Education is Plain Text
link: https://simplystatistics.org/2017/06/13/the-future-of-education-is-plain-text/
link-domain: simplystatistics.org
date: 2017-06-13
---
I totally agree with this article, but I think we should expand this far past
education. Anyone who doesn't own a copy of Microsoft Word knows just
how frustrating it is to receive a `.docx` file from someone. The file opens in
some alternative office suite (for me, Pages) and you're immediately presented
with a list of incompatibilities, as your word processor doesn't support some
archaic edge-cases in the `docx` spec.

Plaintext/Markdown documents make life much simpler. No incompatibilities, no
expensive and proprietary software getting in the way, just content in the
simplest possible format. It's future proof and easily accessible to just about
any device on the planet. Hell, even `.pdf` files are better than sending a
`.docx` or `.ppt`.
