---
title: I've Seen a Future Without Cars, and It's Amazing
categories: microblog
date: 2020-07-09T14:08:08.597Z
link: https://www.nytimes.com/2020/07/09/opinion/ban-cars-manhattan-cities.html
published: false
---
I love the momentum that seems to be forming around taking space away from cars in New York. [This Times article](https://www.nytimes.com/2020/07/09/opinion/ban-cars-manhattan-cities.html) gives me hope that there *is,* in fact, a future where the car is no longer the king of the streets in this city.

Even now, with decreased traffic from the virus walking around the city is a joy. You can hear birds chirping and waves quietly lapping against the shore on the Manhattan Waterfront Greenway. Signs of nature and life that you would never expect to hear on a park situated next to the West Side Highway. Regular streets too are now lined with people dining in outdoor patios built on top of parking spaces. It feels like a whole new (and much more adoreable) streetscape. We're seeing a glimpse of the city New York should have always been.

I've also been pleased with Streetsblog' [recent article](https://nyc.streetsblog.org/2020/07/06/de-blasio-has-punted-so-how-will-the-next-mayor-fix-the-brooklyn-bridge/) listing out each of the 2021 mayoral candidates' opinions on how to address the overcrowding of the Brooklyn Bridge pedestrian walkway. Each of the responses emphasized the need to deprioritize car traffic on the bridge. Imagine how nice it would be for everyone to finally have the space they need to make that crossing enjoyable.

All this to say that I'm cautiously optimistic of what's to come.
