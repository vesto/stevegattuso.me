---
title: Rebuilding my website with Clojure
categories:
- longform
date: 2023-02-03
summary: After a few weeks' worth of work, I've managed to rebuild my website using my own clojure-based static site generator.
---

For some reason whenever I'm in a transitionary point in my life I end up rebuilding my website. Well, this time is no different: I recently quit my job, moved halfway across the world to Germany and, in the spare time between mountains of visa paperwork, have rebuilt my website once more.

Aside from a few UI tweaks (and probably bugs) it shouldn't look too different than it did before. The real change is what is generating the site: a shiny new Clojure-based static site generator I've named Jupi (pronounced HOO-pee, after our cat). I did this partly as a means to gain more experience with Clojure and partly out of frustration with some of the limitations I faced building the previous version of my site with Jekyll. For now, Jupi is pretty specialized to my use case, but here are a few interesting factoids about it:

* The HTML for pages can be generated using nothing but Clojure. I use [hiccup](https://github.com/weavejester/hiccup) to translate the data structures into HTML strings. See the source for [the index page](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/vesto/routes/index.clj) for an example.
* I wanted to experiment with how far I could get by using *only* Clojure, so even [the CSS](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/vesto/routes/stylesheets.clj) is generated with clj using [garden](https://github.com/noprompt/garden). After a few weeks with this I've actually found that I like it more than writing SASS.
* It has the concept of "collections," similar to [Jekyll's](https://jekyllrb.com/docs/collections/), however they're much more flexible and can be defined by anything from markdown files to JPEG files. See examples of [my blog](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/vesto/routes/blog.clj) and [photo galleries](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/vesto/routes/photo_galleries.clj).
* I managed to get a REPL-based live-reloading workflow going using a [highly savage macro](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/jupi/tpl_helpers.clj#L6-L11) that overloads `defn`. Is this a good idea? Maybe not. But it's undeniably *an* idea.
* The photo gallery collection [handles](https://codeberg.org/vesto/stevegattuso.me/src/commit/592f2ea6278d4d8192b377e2febdece1e272dca1/src/vesto/routes/photo_galleries.clj#L25-L51) automatically resizing large photos into thumbnails which should make it much less work to add new galleries in the future.
* The flexibility of having a full programming language at my disposal to generate pages should allow for some fun experimentation. A couple ideas I'm currently floating around:
    * Query my listening history from Plex at compile time and generate a last.fm-like page of my top albums for the month.
    * Fetch posts from Mastodon and archive them on the site.

For those who are curious and want to poke around, I've released the source code [on Codeberg](https://codeberg.org/vesto/stevegattuso.me). I'll likely have more to say on this in the future, as I have a growing desire to clean and package Jupi into a library that others can experiment with.
