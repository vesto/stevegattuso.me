---
layout: post
categories: frontpage microblog
title: "'Our minds can be hijacked': the tech insiders who fear a smartphone dystopia"
link: https://www.theguardian.com/technology/2017/oct/05/smartphone-addiction-silicon-valley-dystopia
link-domain: www.theguardian.com
date: 2017-10-08
---
Really great article discussing the immense amount of power tech companies have
amassed through smartphones and a fun 'lil touch of psychological manipulation.

Yes, I understand that sentence sounds a bit hypobolic/conspiracy-theoryish,
however I agree that tech's power/abilities have been rapidly growing this
decade and is now culminating in the political insanities (eg Brexit/Trump)
we're seeing now.
