---
title: DarkSky makes it to Apple's native weather apps
categories: microblog
date: 2020-06-22T22:28:38.208Z
preview_image: /media/captura-de-pantalla-2020-06-22-a-la-s-18.05.29.png
published: false
---
A small detail from some screenshots in the keynotes (and later confirmed on Apple's website) is adding next-hour precipitation to the native weather apps for both iOS and MacOS:

![](/media/captura-de-pantalla-2020-06-22-a-la-s-18.05.29.png)

![](/media/captura-de-pantalla-2020-06-22-a-la-s-18.06.31.png)

If I could take a guess, it looks like they've already integrated DarkSky's forecasts into iOS. It's a relief for any of us who have been desparately seeking a weather app that has good data + isn't bloated with all sorts of nonsense/distractions, but still a bummer that Dark Sky isn't its own company anymore.
