---
layout: post
categories: frontpage microblog
title: Neuralink and the Brain's Magical Future
link: http://waitbutwhy.com/2017/04/neuralink.html
link-domain: waitbutwhy.com
date: 2017-04-22
published: false
---
I've once again walked away from a Tim Urban article with a completely
different perspective on what humanity's future could turn out to be: this time
with Elon Musk's desire to merge human brains and computers.

One thing I wish he'd have touched on more is just how scary an effective
brain-machine interface would be. If you think privacy advocates are being
paranoid at abuses with today's level of technology, think of where we'll be if
machinery is tied in with our actual brain. Imagine if advertising companies
could get access to granular neural responses to their marketing campaigns. Or
know how much you're thinking about their brand on a daily basis before and
after seeing a particular advertisement. What if tv shows and movies could be
free, so long as you opt-in to allowing advertisers access to your thoughts
while watching? This list could go on for a while, but you get the idea.

Don't get me wrong, I'm super excited about the potential gains we'd see as a
species from having an advanced BMI, and Urban even mentions that these scary
feelings always happen with new technologies that people don't understand. That
being said, the idea of humanity having that level of access to each other's
brains _is fucking terrifying_; a bit moreso than "these kids are [staring at
their phones all day](http://localhost:4000/2013/11/30/a-society-of-phone-zombies.html)
, the world is going down the crapper!" I'm just worried that our lack of
ability to properly manage ethics with today's level of technology is only
going to be amplified as we continue to progress, especially once we get
further down the path of BMIs.

I'm not saying we shouldn't invent the magic wizard hat, simply that we need to
tread very carefully.
