---
layout: post
categories: frontpage microblog
title: TerrariaClone
link: https://github.com/raxod502/TerrariaClone
link-domain: github.com
date: 2017-10-12
---
Anybody who got really excited about programming when they first started
learning can probably relate to this. To date, my most god-awful work was a PHP
clone of 4chan written in a night and maintained over the course of a few
months. If you think you've seen bad code in your life wait until you see a
PHP/MySQL website written by a 10th grader in 6 hours.

The beauty of this Terraria clone is that it's a similar type of ordeal, just
at such a massive scale that it's truly a work of art.
