---
layout: post
categories:
  - frontpage
  - microblog
title: "New Photos: Pandemia NYC"
preview_image: https://www.stevegattuso.me/photos/2020-11-pandemia-nyc/gattuso_steveAA007.jpg
link: https://www.stevegattuso.me/photos/2020-11-pandemia-nyc/
link-domain: stevegattuso.me
---
Pushing up some photos that I recently had developed. Nothing too special, but
some scenes from walking about NYC during the pandemic in the late Fall.
