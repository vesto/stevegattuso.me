---
title: Tactical urbanism in Barcelona
categories:
- longform
date: 2022-06-14
preview_image: https://www.stevegattuso.me/static/media/bcn-2.png
hide_preview_image: yes
---
I recently was in Barcelona visiting and wanted to point out a particularly interesting instance of tactical urbanism I noticed while walking around. Let's take a look at a normal intersection of two streets in the Sant Antoni neighborhood:

![Intersection 1](/static/blog-images/bcn-tactical-urbanism-1.png)
<div class="image-caption">
    Explore <a target="_blank" href="https://www.google.com/maps/@41.3776575,2.1573973,3a,75y,47.21h,92.72t/data=!3m6!1e1!3m4!1sywtGI7--FAi8MS3525WD-A!2e0!7i16384!8i8192">Google Maps</a>.
</div>

Nothing much to see here. It's nice that they have some neighborhood trash bins to properly handle waste but otherwise this intersection is entirely unremarkable. It's a large swath of asphalt with some traffic lights directing vehicular traffic.

Let's take a look at another intersection in the same neighborhood, particularly the intersection of Carrer del Comte Borrell and Carrer de Parlament:

![Intersection 2](/static/blog-images/bcn-tactical-urbanism-2.png)
<div class="image-caption">
    Explore <a target="_blank" href="https://www.google.com/maps/@41.3767413,2.1634492,3a,75y,323.77h,90.07t/data=!3m6!1e1!3m4!1sLDwi_jQZ3ttd5_8U3y4v2g!2e0!7i16384!8i8192">Google Maps</a>.
</div>

Woah- that looks quite a bit different! That large swath of asphalt, previously hostile to all use cases aside from moving cars to and fro, is now a fully pedestrianized space. You can walk through it, bike through it, sit down with a coffee and enjoy the scenery; it's now a space for humans!

What's particularly remarkable about this newly reclaimed space is how little infrastructure it took to create. Look closely: the primary difference between the two intersections is a few wooden planters, some benches, and some paint on the asphalt. This isn't some painstakingly designed and manicured public place- it's just a bunch of furniture scattered around with some paint!

In fact, thanks to Google Street View's ability to look back into history, we can see that this space was initially transformed with even less effort:

![Intersection 2.5](/static/blog-images/bcn-tactical-urbanism-3.png)
<div class="image-caption">
    Explore <a target="_blank" href="https://www.google.com/maps/@41.376734,2.1634481,3a,75y,323.77h,90.07t/data=!3m7!1e1!3m5!1sq2JAhrkv3F-x4VFEi7FpFw!2e0!5s20190301T000000!7i16384!8i8192">Google Maps</a>.
</div>

It doesn't look like much, but a simple line of barricades was all it took to set the stage for a complete transformation of this otherwise unremarkable intersection into a vibrant public space. This technique is known as [tactical urbanism](https://en.wikipedia.org/wiki/Tactical_urbanism): essentially slapping some low-cost features onto an urban space in order to change its shape in a non-committal, yet still impactful way. Pretty cool, right?

As a New Yorker, I walked away from this intersection both frustrated and hopeful for the future of making our streets more liveable. Seeing the transformation from intersection to barricades to public plaza gives me hope that, with the right leadership, programs like Open Streets could be [truly transformative](https://www.transalt.org/open-streets-forever-nyc) in giving streets back to people. Yet at the same time it seems we still have [a long](https://nyc.streetsblog.org/2022/02/10/city-erases-hugely-popular-fort-greene-open-street-allegedly-on-mayors-orders/) [way](https://nyc.streetsblog.org/2022/05/18/willoughbad-progressive-council-member-comes-out-against-popular-open-street/) [to go](https://nyc.streetsblog.org/2021/04/13/caught-amazon-driver-steals-open-streets-barricades-forcing-suspension-of-brooklyn-program/) in changing the backwards, car-centric mindset of our city's citizens and politicians.
