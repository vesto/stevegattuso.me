---
layout: post
categories: frontpage microblog
title: "I Quit Facebook and You Should Too"
link: http://www.erickarjaluoto.com/blog/i-quit-facebook-and-you-should-too/
link-domain: www.erickarjaluoto.com
date: 2017-10-16
published: false
---

The title seems cliché but the author goes through a solid set of reasons for
leaving and, more interestingly, goes into other facets of your life that you
can alter in order to inhibit an addition to technology.

I particularly like the section on controlling his smartphone, as I can
obviously relate to having a group of low-value apps that I periodically check
when I'm bored. If I can increase the amount I read by moving the Kindle app
front and center vs Snapchat that seems like a no-brainer for a low-effort
high-impact change.
