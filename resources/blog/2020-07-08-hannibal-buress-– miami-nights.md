---
title: Hannibal Buress – Miami Nights
categories: microblog
date: 2020-07-08T02:20:50.447Z
link: https://www.youtube.com/watch?v=kVc4-05Agf0
preview_image: /media/img_1424.jpeg
published: false
---
In an odd coincidence, one of my favorite comedians released a new special, *Miami Nights*, just as I flee from Miami back to New York. At some point I'd like to write a bit about my brief experience of living there from the prospective of an aggressively anti-car urbanist. Unfortunately being in-between apartments during a pandemic isn't the best environment to sit down, think and write.

Anyways, I'd highly recommend watching this special if you're looking for a pick-me-up in this hellscape of a year.
