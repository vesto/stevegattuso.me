---
layout: post
categories: frontpage microblog
title: Magic Wormhole
link: https://github.com/warner/magic-wormhole
link-domain: github.com
date: 2017-02-05
---
There are a million bajillion different ways to transfer files between 
computers, but each seems to have its own quirks that make it not quite the 
right solution for the particular use case. This is especially the case when
transferring sensitive data like passwords, API keys, etc.

Magic Wormhole seems to be a really well thought out solution to this problem.
It offers a secure medium (using password-authenticated key exchange) while
also managing to have a super easy to use CLI. Just `wormhole send
secrets.txt`, tell your friend the magic code (something like
`3-cake-ambiguous`), and they can receive it by running `wormhole receive`.
Super elegant!
