---
title: Put tiny businesses back into residential neighborhoods
categories: microblog
date: 2020-07-24T12:17:17.062Z
link: https://www.fastcompany.com/90530672/with-downtowns-staying-abandoned-put-tiny-businesses-back-into-residential-neighborhoods
---
It's no secret that we need to find ways to move American suburbs away from automobile dependence. The idea of peppering sparse neighborhoods with these small shops sounds wonderful, from both environmental and social perspectives.

**Environmental**: Less dependence on cars! The more walkable neighborhoods are, the less car trips are required to perform common tasks. How many suburban neighborhoods have you been to that have any sort of shop or restaurant within reasonable walking distance? For many people, *any* sort of trip is impossible without their car.

**Social**: Cheaper rents means it's easier for mom 'n pop shops to sprout up, experiment with new ideas, and create actual character in a neighborhood. An easy way city people dunk on American suburbs is how cookie-cutter they are; generally being filled with nothing but national chains. Cute shops, restaurants, and cafes nestled away amongst previously-sparse neighborhoods are the antidote to suburban monotony.

These kinds of businesses can be anchors of neighborhoods, providing a place for people to meet their neighbors, bump into friends, and more generally interact with other humans (which is a good thing!). These are interactions that are otherwise pretty limited if everybody is going through the drive-thru window on their way to/from work.

In any case, if we want to start getting serious about protecting the environment (and rebuilding our sense of community), we need to start implementing ideas like these ASAP in order to retrofit our suburbs into something more sustainable.