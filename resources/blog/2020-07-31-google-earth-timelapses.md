---
title: Google Earth Timelapses
categories: microblog
date: 2020-07-31T13:53:32.251Z
link: https://earthengine.google.com/timelapse/
published: false
---
Things are crazy right now, especially here in the United States. I've been hanging in by a thread lately, but  one thing that's been keeping me going is the thought that the ongoing public health, economic, and political crisis all have a silver lining: there is the potential to make big changes right now.

Take a look at these timelapses on Google Earth. If this doesn't inspire you to do *something* towards trying to conserve the environment, I don't know what will.

Some ideas:

* Donate to a local conservation/environmental activist group.
* If you have a car, try substituting one car trip to use public transit, a bike, or some kind of micromobility vehicle (ie an electric scooter).
* Write to your local representative about enacting some sort of pro-environment policies. Anything from increasing funding of public transit, blocking a pipeline (esp those of us in [North Brooklyn](https://www.saneenergy.org/nonbkpipeline)) to planting trees around your town.
* If you're really wanting to dive in: consider dedicating your career to the cause (we're hiring at [Amperon](https://angel.co/company/amperon/jobs)), or moving to a place that doesn't rely on cars as the primary form of transit.

Sorry if this comes off as preachy, but for some reason these timelapses got to me this morning.
