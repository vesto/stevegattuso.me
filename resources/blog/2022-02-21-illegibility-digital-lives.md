---
title: Illegibility in our digital lives
categories:
- longform
date: 2022-02-21
---

I picked up James C. Scott's _[Seeing Like a State](https://en.wikipedia.org/wiki/Seeing_Like_a_State)_ recently and, while I haven't gotten too deep into it yet, I found its ideas of _social legibility_ particularly interesting. The author argues that states must make their societies _legible_ in order to more efficiently control, tax, and more broadly impose their will upon individuals. Legibility can manifest itself in many ways: a census, tax returns, even city grids (as opposed to the organic and chaotic street layouts found in older cities). This legibility can be used for good or bad: to control the population or to more efficiently create social programs to benefit people.

Since learning about this idea I keep thinking about how it applies to our digital lives. As people's online interactions have shifted towards centralized platforms: Instagram, Google, Twitter, TikTok, Reddit, etc., they have become increasingly _legible_ and, if you buy the arguments of _Seeing Like a State_, more control-able.

More than enough has been written about the implications of these giant platforms controlling and monitoring our lives. One thing I haven't read much of is how states and companies' reliance on social legibility can be used as a tool against them.  It seems like there are a variety of simple acts that can throw a wrench into the legibility of one's digital life in a meaningful way. A few examples off of the top of my head:[^2]

* Using uBlock or running a pi hole on your local network to block tracking scripts and ads.
* Using Linux instead of MacOS or Windows.
* Hosting communities on platforms like [Mastodon](https://joinmastodon.org/), [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat), or [Matrix](https://matrix.org/).
* Using an alternative frontend to social networks like [Invidious](https://github.com/iv-org/invidious) (YouTube), [Nitter](https://github.com/zedeus/nitter) (Twitter) or [Teddit](https://codeberg.org/teddit/teddit) (Reddit).[^1]
* Publishing on your own website, especially if you use a protocol like [Gemini](https://en.wikipedia.org/wiki/Gemini_(protocol)) (a simplified HTTP alternative).

I've been exploring a lot of these alternative digital spaces lately, mostly for the fun of it all and the wholesome communities I've found around them. From reading _Seeing Like a State_ I'm realizing that these also function as a form of resistance against the hyper-commercialized internet I find myself regularly frustrated by.

Thinking about it- I've also realized how effective being an outlier in data can be. I frequently work with large datasets and know how frustrating it is to find oddities and outliers. Many times I simply remove these points in order to keep my abstractions simpler and easier to understand!

My hope is that by finding these small ways of increasing my _illegibility_, I can ensure the breadcrumbs of my digital life can be outliers- on the fringes of the norm. The hope is that these fringes aren't worth it for large companies to attempt to normalize, commercialize, or control. It's a small yet somehow satisfying form of personal resistance.

[^1]: See also: [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect), which can automatically redirect you to each of these alternate frontends.
[^2]: If you can think of more fun ones ping me on [Mastodon]({{site.mastodon_url}}) or [email](mailto:steve@stevegattuso.me). I'm thinking about putting up a list of them on my wiki at some point.
