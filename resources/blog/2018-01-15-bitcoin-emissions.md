---
layout: post
categories: frontpage microblog
title: Bitcoin emissions per transaction
date: 2018-01-15
---

An interesting statistic I've found: according to Digiconomist's [Bitcoin
energy consumption](https://digiconomist.net/bitcoin-energy-consumption) page,
the average carbon footprint of each transaction made is 162.48kg of CO2.

Based on Carbon Independent's [aviation emissions estimates](http://www.carbonindependent.org/sources_aviation.html)
of 26.6g of CO2 per passenger km, we can come up with a rough estimate of **each
bitcoin transaction being the equivalent of a 6,108km (3,795mi) flight**. This
is more or less the distance of a flight from New York City to Copenhagen,
Denmark (calculated using [DistanceFromTo](https://www.distancefromto.net/)).
