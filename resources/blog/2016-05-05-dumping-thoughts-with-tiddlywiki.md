---
layout: post
title: Dumping thoughts with TiddlyWiki
summary: How I switched from vimwiki to TiddlyWiki to keep track of my everyday thoughts and ideas.
category: frontpage
date: 2016-05-05
slug: dumping-thoughts-with-tiddlywiki
---
I recently discovered [TiddlyWiki](http://tiddlywiki.com) while searching
around for an alternative for my current brain dumping solution, vimwiki. I 
really enjoy having a place where I can jot down notes, keep a personal
journal, and just write for my future self to read. For the last 4 years
vimwiki has been my pick for this. It's simple, easy to use, and let me keep
my thoughts in portable text files that I could parse and back up easily.

Unfortunately this ease came with quite a few restrictions. First off, there
was no great way for me to edit the wiki on a mobile device; it was limited to
my computer only. Secondly, since it was built into a terminal editor from the
70s, there was no way to embed any sort of rich media or interactivity. It was
just text. For many people this isn't a problem, as the only thing they're
looking for is to keep a simple text journal. For the longest time I was on the
same boat, however it turns out that I didn't quite know what I was missing out
on.

In comes TiddlyWiki, described as "a unique non-linear notebook for capturing,
organising and sharing complex information." Rather than being in a 70s
terminal, TiddlyWiki runs within your web browser. This allows articles to embed
images, videos, text, and even interactive widgets (implemented in javascript).
So far so good, but what about durability? One of my favorite aspects of
vimwiki was that its "database format" was just text files, ensuring that there
was zero lock in. TiddlyWiki advertises that it lets you choose how to store
your data, "guaranteeing that in the decades to come you will still be able to
use the notes you take today." I was skeptical at first, as their normal method
of saving consists of re-saving itself as a single html file that you download.
Luckily tiddlywiki5 adds a node-based command line tool which launches a server
for the wiki, allowing each *tiddler* (their word for article) to be saved as
its own text file with a simple format:

    created: 20160419022137188
    creator: user
    modified: 20160419022316450
    modifier: user
    order: 1.9
    tags: sidebar
    title: Projects
    type: text/vnd.tiddlywiki

    Article text is everything on this line and below.

As you can see, it's pretty straightfoward. A simple key-value section at the
top (similar to YAML), and the article text below. In this case the `order` key
is a custom value I use in my wiki (you can attach these arbitrary key/values
freely to tiddlers), another flexible feature that convinced me to make the
switch.

After seeing all of these advantages over vimwiki I decided to make a full
switch over. I wrote a quick script which migrated all of my existing vimwiki
journal entries into tiddlywiki's format and was up and running with my brand
new wiki in around a half hour. As I was setting up a server for my iPhone to
access the wiki I noticed my first annoyance with this new system: the only
method of authentication on the node server was using HTTP basic. I found this
to be slightly annoying due to web browsers' built-in login prompts lacking a
"remember me" function, forcing me to log in each time I wanted to write. This
is a problem, as one of the major selling points for this new wiki was lowering
the barrier to write, ideally encouraging me to write down ideas and thoughts
more often.

To solve this, I decided to take matters into my own hands and write a small
Ruby application called [twproxy](https://github.com/stevenleeg/twproxy). It
acts as a lightweight reverse-proxy which adds a cookie-based authentication
method to protect the wiki behind it. If you're messing around with Tiddlywiki
and are thinking about using it on a publicly exposed server, this might be the
solution to making sure it stays protected. I've been using it for the last
month or so without issues and have been happy that I've finally written some
code that I use myself on a daily basis.

All in all, I'd highly recommend Tiddlywiki for those who are either interested
in a more functional wiki or for those who have never had a personal wiki
before. I've found having a place to dump my thoughts and ideas to be immensely
valuable from both a utilitarian and nostalgic standpoint. The added niceties
TiddlyWiki provides has done a great job of making my writing more interesting
and meaningful, something that I'm entirely grateful for and gladly recommend
to others.

