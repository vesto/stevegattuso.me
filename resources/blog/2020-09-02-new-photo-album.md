---
title: "New photo album: South America (Feb '20)"
categories: microblog
date: 2020-09-02T21:33:32.895Z
link: https://www.stevegattuso.me/photos/2020-02-south-america/
preview_image: /media/27180018.jpeg
---
I was lucky enough to be able to finish a trip to South America with some friends *just* before the COVID-19 lockdowns began around the world. This is an album of photos I took on my Kodak Euro-35 throughout the trip, passing through Montevideo, Buenos Aires, Santiago, and the Atacama desert in Chile (where most of these photos were taken).

This was the first time I’d used this camera with color film and for landscape photography, and needless to say the result wasn’t phenomenal. This particular camera is a point-and-shoot, and seems to be best at capturing subjects reasonably nearby and in brighter environments.

Either way, I’ve decided to post these because I like how the some photos look as if they’re old NASA images from another planet. Enjoy!
