---
layout: post
categories: frontpage microblog
title: From OSX to Ubuntu
link: https://nicolas.perriault.net/code/2016/from-osx-to-ubuntu/
link-domain: nicolas.perriault.net
date: 2017-01-14
---
I've seen an uptick in popularity of these OS X to Linux articles in the last
few months since the rise of the touchbar Macbook Pros. While I'm always happy
to see more people diving into the world of desktop Linux, I've still yet to 
be able to stick with it myself.

My latest experiment with making the switch (this time to [Ubuntu GNOME](https://ubuntugnome.org/))
lasted ~3-4 weeks until a kernel upgrade caused my computer to stop booting. I
was really happy with everything else about the experience, but dealing with
debugging an unbootable OS when you're trying to get work done is a showstopper
for me. From my experience macOS still appears to be the most stable option for
getting what I need done, but I'm sure I'll end up giving Linux another chance
in the near future.
