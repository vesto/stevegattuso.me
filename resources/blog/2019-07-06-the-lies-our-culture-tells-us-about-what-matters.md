---
layout: post
categories: frontpage microblog
title: "The lies our culture tells us about what matters -- and a better way to live"
link: https://www.youtube.com/watch?v=iB4MS1hsWXU
link-domain: youtube.com
date: 2019-07-06
slug: the-lies-our-culture-tells-us-about-what-matters
published: false
---
Something I've felt (and disliked) about American society is our desire to be
self-sufficient as individuals. We don't want to share with one another; we
want our own and we want it to be better than everyone else's. David Brooks, an
op-ed columnist at the New York Times does an amazing job of explaining how
this mentality has failed us, and gives advice of what we can do to fix it.

Somewhere in my head is a much longer essay about why I think this is so
important, using the American preference of cars over public transit as an
example of how we've lost sight of what we can achieve when we band together.
