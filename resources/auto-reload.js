(() => {
  window.addEventListener('load', () => {
    let lastUpdated = -1;

    console.log('Watching for updates...');
    window.setInterval(() => {
      fetch('/_internal/update-timestamp')
        .then((r) => r.text())
        .then((newTimestamp) => {
          if (lastUpdated !== -1 && lastUpdated !== newTimestamp) {
            console.log('Update detected. Reloading');
            location.reload();
          }

          lastUpdated = newTimestamp;
        });
    }, 2000);
  });
})();
