(ns vesto.routes.wiki-books
  (:require [clj-yaml.core :as yaml]
            [hiccup.core :refer [html]]
            [markdown.core :as md]
            [jupi.tpl-helpers :refer [defn]]
            [vesto.layouts.page]))

(defn render-book [b]
  [:div.inventory--book
   [:div.inventory--book-metadata
    [:i
     [:a {:href (:url b)} (:title b)]]
    (str " by " (:author b))
    (map #(vector :span.inventory--book-tag %) (:tags b))]
   (if (contains? b :comments)
     [:div.inventory--book-comments
      (md/md-to-html-string (:comments b))]
     nil)])

(defn render [p s]
  (let [data (yaml/parse-string (slurp "resources/data/books.yaml"))]
    (html
      [:p (md/md-to-html-string (:intro data))]
      [:div.inventory--books
       (map render-book (:books data))])))

(defn route []
  {:route/type :page
   :route/emit-path "wiki/books.html"
   :route/metadata {:title "Books"
                    :nav ["Wiki" "Books"]
                    :created-at "2022-06-01"
                    :updated-at "2023-02-25"
                    :wiki-id "books"
                    :src-path *file*}
   :page/render render
   :page/layout vesto.layouts.page/render})
