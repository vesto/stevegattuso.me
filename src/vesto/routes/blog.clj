(ns vesto.routes.blog
  (:require [babashka.fs :as fs]
            [hiccup.core :refer [html]]
            [clojure.string :as string]
            [vesto.utils :as utils]
            [vesto.layouts.base]
            [jupi.tpl-helpers :as tpl :refer [defn]]))
            
(defn render-item [post site]
  (let [post-html (->> (:collection/item/path post)
                       (str)
                       (slurp)
                       (utils/parse-md site)
                       (:html))
        link (tpl/route-md post :link)]
    (html
      [:div.content-container.text-content
       (if link
         [:div.page--title
          [:a {:href link :target "_blank"} "🔗 " (tpl/route-md post :title)]]
         [:div.page--title (tpl/route-md post :title)])
       [:div.page--meta
        (tpl/format-date "MMMM d, yyyy" (tpl/route-md post :date))]
       [:div.page--md-content
        (when (tpl/route-md post :preview-image)
          [:img {:src (tpl/url-for site (tpl/route-md post :preview-image))}])
        post-html]])))

(defn render-metadata [path]
  (let [frontmatter (tpl/fetch-md-frontmatter path)
        split-filename (-> (fs/file-name path)
                           (string/split #"\-"))
        date (->>
               (take 3 split-filename)
               (map #(Integer/parseInt %))
               (string/join "-")
               (.parse (java.text.SimpleDateFormat. "yyyy-MM-dd")))
        slug (as->
               (drop 3 split-filename) x
               (string/join "-" x)
               (string/replace x #"\.md" ""))]
    (merge frontmatter {:date date
                        :slug slug
                        :nav ["Blog" (:title frontmatter)]})))

(defn render-emit-path [post]
  (let [slug (tpl/route-md post :slug)
        fmt-date (tpl/format-date "yyyy/MM/dd" (tpl/route-md post :date))]
    (str fmt-date "/" slug ".html")))

(defn route []
  {:route/type :collection
   :collection/identifier "blog"
   :collection/item-generator (tpl/generate-md-items "resources/blog")
   :collection/layout vesto.layouts.base/render
   :collection/render-metadata render-metadata
   :collection/render-emit-path render-emit-path
   :collection/render-item render-item})
