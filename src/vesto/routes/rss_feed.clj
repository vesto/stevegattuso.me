(ns vesto.routes.rss-feed
  (:require [hiccup.core :refer [html]]
            [clj-yaml.core :as yaml]
            [jupi.tpl-helpers :as tpl :refer [defn]]))

(def rfc-fmt "EEE, dd MMM yyyy HH:mm:ss")

(defn render-item [item site]
  (let [md
        (cond
          (= "blog" (:collection/identifier item))
          {:url (tpl/url-for site item)
           :title (tpl/route-md item :title)
           :date (tpl/route-md item :date)
           :desc (if (tpl/route-md item :summary)
                   (tpl/route-md item :summary)
                   "New blog post published.")}

          :else
          {:url (tpl/url-for site (first (:paths item)))
           :title "New update to website"
           :date (:date item)
           :desc (:msg item)})]
    [:item
     [:title (:title md)]
     [:link (:url md)]
     ; TODO: If you're using the changelog again you need to adjust this,
     ; as the same URL can show up multiple times which breaks the linter.
     [:guid (:url md)]
     [:pubDate (str (tpl/format-date rfc-fmt (:date md)) " UTC")]
     [:description (:desc md)]]))

(defn extract-date [item]
  (if (some? (:collection/identifier item))
    (tpl/route-md item :date)
    (:date item)))

(defn render [p s]
  ; TODO: Maybe make a separate feed that adds in the changelog? Don't do it
  ; here, as you'll mess with the RC bloggerator
  (let [posts (->> (filter #(= (:collection/identifier %) "blog") s)
                   (sort-by extract-date)
                   (reverse)
                   (take 10))]
    (html
      {:mode :xml}
      [:rss {:version "2.0"
             :xmlns:atom "http://www.w3.org/2005/Atom"}
       [:channel
        [:title "Steve Gattuso"]
        [:link (tpl/url-for s "/")]
        [:description "Steve Gattuso's blog"]
        [:webMaster "steve@stevegattuso.me (Steve Gattuso)"]
        [:lastBuildDate
         (str (tpl/format-date rfc-fmt (java.util.Date.)) " UTC")]
        (map #(render-item % s) posts)]])))


(defn route []
  {:route/type :page
   :route/emit-path "feed.xml"
   :page/render render})
