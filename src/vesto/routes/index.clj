(ns vesto.routes.index
  (:require [hiccup.core :refer [html]]
            [markdown.core :as md]
            [clj-yaml.core :as yaml]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [vesto.routes.stylesheets :as style]
            [vesto.utils :refer [wiki-url wiki-link]]
            [vesto.layouts.base]))

(defn render-blog-preview [s]
  (let [all-posts (filter #(= (:collection/identifier %) "blog") s)
        latest (->> all-posts
                    (sort-by #(tpl/route-md % :date))
                    (reverse)
                    (take 5))]
    [:div.index--list-box
     [:ul.dated-list
      (map (fn [post]
             (let [date (tpl/route-md post :date)
                   title (tpl/route-md post :title)
                   link (:route/emit-path post)]
               [:li [:span.dated-list--date
                     (tpl/format-date "yyyy-MM-dd:" date)]
                [:a {:href (tpl/url-for s link)} title]])) latest)]
     [:p
      "See all " (count all-posts) " posts in "
      [:a {:href (tpl/url-for s "blog")} "the archive &raquo;"]]]))

(defn render-changelog [s]
  (let [log (->> (slurp "resources/data/changelog.yaml")
                 (yaml/parse-string)
                 (take 5))]
    [:div.index--list-box
     [:ul.dated-list
      (map (fn [entry]
             [:li
              [:span.dated-list--date
               (tpl/format-date "yyyy-MM-dd:" (:date entry))]
              [:a {:href (wiki-url s (:id entry))} (:msg entry)]]) log)]]))

(defn render [p s]
  (html
    [:div.content-container.index--container
     [:h1 "Hey!"]
     [:p "Welcome to my website 🕸️ 👀, a wiki-style place for me to share what I'm thinking about, listening to, taking photos of, etc. There's a lot going on here so if you're visiting for the first time and want to skip to the good stuff, check out " (wiki-link s "the-hits") "."]

     [:h1 "Blog"]
     [:p "I like writing about technology, cities, digital privacy, and sometimes even politics. If you like what you see here you can subscribe to my " [:a {:href (tpl/url-for s "/feed.xml")} "RSS feed"] " or follow me on Mastodon."]
     (render-blog-preview s)
     
     [:h1 "Changelog"]
     (render-changelog s)]))

(def mobile-container
  (style/on-mobile
    {:padding-left (:mobile-margin style/sizes)
     :padding-right (:mobile-margin style/sizes)
     :box-sizing "border-box"}))

(defn render-stylesheet [s]
  [[:.index--container
    [:h1 mobile-container]
    [:p
     {:line-height "1.5em"
      :margin "10px 0px"}
     mobile-container]]

   [:.index--list-box
    {:background-color (style/colors :white)
     :padding "15px"
     :margin-bottom "15px"}
    mobile-container
    [:ul.dated-list
     {:margin "0px"}]
    [:p
     {:margin "5px 0px 0px 0px"}
     (style/on-mobile
       {:padding "0px"})]]])

(defn route []
  {:route/type :page
   :route/emit-path "index.html"
   :route/metadata {:title "Index"
                    :nav ["Home"]
                    :stylesheet/render render-stylesheet}
   :page/render render
   :page/layout vesto.layouts.base/render})
