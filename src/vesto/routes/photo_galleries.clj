(ns vesto.routes.photo-galleries
  (:require [babashka.fs :as fs]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [clj-yaml.core :as yaml]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [jupi.core :as jupi]
            [image-resizer.core :as resizer]
            [image-resizer.format :as fmt]))

(defn generate-static-path
  "Given the source path of a photo (eg in resources/photo-galleries), spit out the path it'll have once it's been copied to the static directory."
  [gal src-path]
  (str "static/photo-galleries/"
       (tpl/route-md gal :slug) "/"
       (fs/file-name src-path)))

(defn generate-thumbnail-path
  "Similar to static path but for the thumbnail"
  [gal src-path]
  (str "static/photo-galleries/"
       (tpl/route-md gal :slug) "/thumbs/"
       (fs/file-name src-path)))
  
(defn render-photo!
  "Given the path to a photo, generate a thumbnail and render it out to the static path to be served."
  [gal path]
  (let [parent-dir (fs/parent (generate-static-path gal path))
        photo-name (fs/file-name path)
        slug (tpl/route-md gal :slug)
        thumb-cache-path (jupi/get-cache-path
                           (str "gallery-thumbs/" slug "/" photo-name))
        thumb-output-path (jupi/get-output-path
                            (str parent-dir "/thumbs/" photo-name))
        orig-output-path (jupi/get-output-path
                           (generate-static-path gal path))
        orig-f (clojure.java.io/file (str path))]

    ; Make sure the destination paths exist
    (io/make-parents orig-output-path)
    (io/make-parents thumb-output-path)
    (io/make-parents thumb-cache-path)

    (if (fs/exists? thumb-cache-path)
      (fs/copy thumb-cache-path thumb-output-path)
      (do
        ; Resize original and save thumbnail
        (println (str "Generating thumbnail for " slug "/" photo-name))
        (let [resized (resizer/resize orig-f 800 800)]
          (fmt/as-file resized thumb-output-path :verbatim)
          (fmt/as-file resized thumb-cache-path :verbatim))))

    (fs/copy path orig-output-path)))

(defn render-preview [s gal photo-path]
  (let [filename (fs/file-name photo-path)
        static-path (generate-static-path gal photo-path)
        thumb-path (generate-thumbnail-path gal photo-path)
        out-path (tpl/url-for s thumb-path)]
  [:a.gallery--item
   {:style (str "background-image: url('" out-path "')")
    :href (tpl/url-for s static-path)}]))

(defn render-item [gal site]
  ; We're going to throw in some side effects here because why the hell not?
  (doall
    (map #(render-photo! gal %) (tpl/route-md gal :photos)))

  ; Aaaand finally, return the actual HTML
  [:div.content-container.photo-gallery
   [:div.page--title (tpl/route-md gal :title)]
   [:div.page--meta (tpl/route-md gal :description)]
   (when (tpl/route-md gal :story)
     [:p (tpl/route-md gal :story)])
   [:div.gallery--items
    (map #(render-preview site gal %) (tpl/route-md gal :photos))]])

(defn render-metadata [path]
  (let [yaml-data (->> (str path "/metadata.yaml")
                       slurp
                       yaml/parse-string)
        photos (->> (fs/list-dir path)
                    (filter #(not= (fs/file-name %) "metadata.yaml")))]
    (merge yaml-data {:slug (fs/file-name path)
                      :photos photos
                      :nav ["Photos" (:title yaml-data)]})))

(defn render-emit-path [p] 
  (let [filename (fs/file-name (:collection/item/path p))
        emit-slug (string/replace filename #"\.md" "")]
    (str "photos/" emit-slug "/index.html")))

(defn item-generator []
  (->> (fs/list-dir "resources/photo-galleries")
       (filter #(fs/exists? (str % "/metadata.yaml")))))

(defn route []
  {:route/type :collection
   :collection/identifier "photo-gallery"
   :collection/item-generator item-generator
   :collection/layout vesto.layouts.base/render
   :collection/render-metadata render-metadata
   :collection/render-emit-path render-emit-path
   :collection/render-item render-item})
