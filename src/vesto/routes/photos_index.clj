(ns vesto.routes.photos-index
  (:require [hiccup.core :refer [html]]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [vesto.routes.stylesheets :as style]
            [vesto.layouts.base]))

(defn generate-static-path
  "Given the source path of a photo (eg in resources/photo-galleries), spit out the path it'll have once it's been copied to the static directory."
  [gal cover]
  (str "static/photo-galleries/"
       (tpl/route-md gal :slug) "/thumbs/"
       cover))

(defn render-gallery [s gal]
  (let [cover-path (generate-static-path gal (tpl/route-md gal :cover))]
    [:a.gallery--item
     {:style (str "background-image: url('" (tpl/url-for s cover-path) "')")
      :href (tpl/url-for s (str "photos/" (tpl/route-md gal :slug)))}
     [:div.gallery--item-meta
      [:div.gallery--item-name (tpl/route-md gal :title)]
      [:div.gallery--item-desc (tpl/route-md gal :description)]]]))

(defn render [p s]
  (let [galleries
        (->> s
             (filter #(= (:collection/identifier %) "photo-gallery"))
             (sort #(compare (tpl/route-md %1 :slug)
                             (tpl/route-md %2 :slug)))
             (reverse))]
    (html
      [:div.content-container.text-content
       [:h1 "Photos"]
       [:p "I like traveling a lot which also means I sometimes accumulate some worthwhile photos to share. Many of these photos are taken on film using a " [:a {:href "https://en.wikipedia.org/wiki/Canon_A-1"} "Canon A1"] " or " [:a {:href "http://camera-wiki.org/wiki/Kodak_S100"} "Kodak Euro-35."]]
       [:div.gallery--items
        (map #(render-gallery s %) galleries)]])))

(defn route []
  {:route/type :page
   :route/emit-path "photos/index.html"
   :route/metadata {:title "Photos"
                    :nav ["Photos"]}
   :page/render render
   :page/layout vesto.layouts.base/render})
