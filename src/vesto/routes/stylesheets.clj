(ns vesto.routes.stylesheets
  (:require [garden.core :refer [css]]
            [garden.stylesheet :as stylesheet]
            [garden.color :as color]
            [garden.def :refer [defcssfn]]
            [garden.selectors :as sel]
            [garden.util :as gutil]
            [jupi.tpl-helpers :as tpl :refer [defn]]))

;;;;
;; Variables
;;
(def colors
  (let [base-colors
        {:white (color/hex->rgb "#FFF")
         :green "#5EBD3E"
         :yellow "#FFB900"
         :orange "#F78200"
         :red "#E23838"
         :purple "#973999"
         :blue "#009CDF"
         :dark (color/hex->rgb "#1d1d1d")
         :beige "#F4EAD5"}

        derived-colors
        {:off-white (color/darken (:white base-colors) 4)
         :alt-text (color/lighten (:dark base-colors) 50)
         :border (color/darken (:white base-colors) 10)}]
    (merge base-colors derived-colors)))

(def sizes {:content-width "775px"
            :border-radius "5px"
            :mobile-margin "10px"})

(defcssfn url)
(defn css-url [site path] (url (tpl/url-for site path)))

;;;;
;; Helpers
;;
(def font-serif {:font-family "Lora, serif"})
(def font-sans {:font-family "Montserrat, sans-serif"})
(def font-mono {:font-family "Andale Mono, monospace"})

(defn on-mobile [body]
  (stylesheet/at-media
    {:max-width (:content-width sizes)}
    [:& body]))

;;;;
;; Stylesheets
;;
(defn render-inventory [site]
  [[:.inventory--book
    {:margin-bottom "10px"}

    [:.inventory--book-metadata
     {:font-size "18px"}]

    [:.inventory--book-comments
     {:margin-top "10px"
      :font-size "14px"
      :border-left-width "2px"
      :border-left-style "solid"
      :border-left-color (:border colors)
      :padding "5px 15px"
      :max-width "600px"}
     
     [:p
      {:margin "0px"
       :padding "0px"
       :line-height "1.3em"}]]
    
    [:.inventory--book-tag
     {:font-size "12px"
      :background (:off-white colors)
      :border-radius "3px"
      :padding "2px 4px"
      :margin-left "3px"}]]])

(defn render-main [site]
  (let [inline-sheets
        (->> site
          (filter #(some? (tpl/route-md % :stylesheet/render)))
          (map #(apply (tpl/route-md % :stylesheet/render) [site])))]
    (css
      (stylesheet/at-font-face
        {:font-family "Lora"
         :font-style "normal"
         :font-weight 400
         :src [(css-url site "static/fonts/lora-v15-latin-regular.woff2")
               (css-url site "static/fonts/lora-v15-latin-regular.woff")]})
      (stylesheet/at-font-face
        {:font-family "Lora"
         :font-style "normal"
         :font-weight 700
         :src [(css-url site "static/fonts/lora-v15-latin-700.woff2")
               (css-url site "static/fonts/lora-v15-latin-700.woff")]})
      (stylesheet/at-font-face
        {:font-family "Lora"
         :font-style "italic"
         :font-weight 400
         :src [(css-url site "static/fonts/lora-v15-latin-italic.woff2")
               (css-url site "static/fonts/lora-v15-latin-italic.woff")]})
      (stylesheet/at-font-face
        {:font-family "Lora"
         :font-style "italic"
         :font-weight 700
         :src [(css-url site "static/fonts/lora-v15-latin-700italic.woff2")
               (css-url site "static/fonts/lora-v15-latin-700italic.woff")]})

      [:body
       ; TODO: Include small mixin
       font-serif
       {:background-color (:off-white colors)
        :color (:dark colors)
        :margin "0px"
        :padding "0px"
        :font-size "16px"
        :min-width (:content-width sizes)
        :display "flex"
        :flex-direction "column"
        :min-height "100vh"}

       (on-mobile
         {:width "100vw"
          :min-width "auto"})

       [:&.dark
        {:background-color (:dark colors)
         :color (:off-white colors)}

        [:header
         {:background-color "#2D2731"}
         [:.site-title :span
          {:color (:off-white colors)}]
         [:nav :a
          {:color (:off-white colors)}]]

        [:nav :ul.nav :a.selected
         {:color (:off-white colors)}]]]

      [:h1 {:font-weight "normal"
            :font-size "32px"
            :margin-top "0px"
            :margin-bottom "10px"}]
      [:a
       {:color (:blue colors)
        :cursor "pointer"
        :text-decoration "none !important"}
       [:&:hover
        {:text-decoration "underline"}]
       [:&.wiki-link:hover
        {:text-decoration "none"
         :background-color (:blue colors)
         :color (:off-white colors)}]
       ["&.wiki-link::before"
        {:content (gutil/wrap-quotes "{")
         :display "inline-block"
         :font-size "18px"
         :padding-right "3px"}]
       ["&.wiki-link::after"
        {:content (gutil/wrap-quotes "}")
         :display "inline-block"
         :font-size "18px"
         :padding-left "3px"}]
       ["&.wiki-link:hover::before" "&.wiki-link:hover::after"
         {:color (:white colors)}]]

      [:header
       {:background-color (:beige colors)
        :padding "20px 0px"
        :text-align "left"
        :box-sizing "border-box"}

       (on-mobile
         {:padding "5px 0px"})

       [:.content-ontainer
        {:display "flex"
         :justify-content "space-between"
         :align-items "center"}

        (on-mobile
          {:display "flex"
           :flex-direction "column"
           :padding-left (:mobile-margin sizes)
           :padding-right (:mobile-margin sizes)
           :box-sizing "border-box"})]

       [:.site-title
        {:display "block"
         :font-size "32px"
         :text-decoration "none"
         :font-weight "normal"
         :text-transform "lowercase"
         :color (:blue colors)
         :margin "0px"}
        [:span
         {:color (:dark colors)}]

        (on-mobile
          {:text-align "center"})]

       [:.title-container
        {:display "flex"
         :align-items "center"
         :justify-content "space-between"
         :flex-direction "row"}

        (on-mobile
          {:flex-direction "column"})]

       [:.meta
        {:display "flex"
         :flex-direction "row"
         :margin-top "10px"}
        (on-mobile
          {:flex-direction "column"
           :margin-top "0px"})

        [:.photo
         {:min-width "100px"
          :width "100px"
          :height "100px"
          :background-image (css-url site "static/images/me.jpg")
          :background-size "contain"
          :background-position "center center"
          :border-radius "50px"
          :margin-right "15px"
          :filter "grayscale(1)"}
         (on-mobile
           {:display "none"})]

        [:.bio
         {:flex 1
          :line-height "1.5em"}

         (on-mobile
           {:text-align "center"
            :font-size "14px"
            :padding "0px 10px"})]]]

      [:.top-links
       {:display "flex"}
       (on-mobile
         {:margin-top "10px"})

       [:ul
        {:display "flex"
         :list-style "none"
         :margin "0px"
         :padding "0px"}
        [:li
         {:margin "0px"
          :padding "0px"}]
        [:a
         {:text-decoration "none"
          :padding "5px 10px"
          :transition "150ms"
          :color (:dark colors)}
         [:&:hover
          {:background (:blue colors)
           :color (:beige colors)
           :border-radius "3px"}]]]]

      [:.stripe
       [:div
        {:height "3px"}
        (->> [:green :yellow :orange :red :purple :blue]
             (map-indexed
               #(vector (sel/& (sel/nth-child (+ 1 %1))) (hash-map :background-color (%2 colors)))))]]

      [:.primary-nav
       {:background-color (:white colors)
        :margin-bottom "15px"
        :border-bottom-color (:border colors)
        :border-bottom-width "0.5px"
        :border-bottom-style "solid"
        :padding "10px 0px"}
       (on-mobile
         {:padding-left (:mobile-margin sizes)
          :padding-right (:mobile-margin sizes)})

       [:ul
        {:width (:content-width sizes)
         :margin "0px auto"
         :padding "0px"
         :list-style "none"
         :display "flex"
         :line-height "1.7em"}
        (on-mobile
          {:width "100%"
           :flex-wrap "wrap"})]

       [:li
        [:.divider
         {:display "inline-block"
          :width "33px"
          :text-align "center"
          :color (:alt-text colors)}]
        [(sel/& (sel/nth-last-child "1"))
         [:.divider
          {:display "none"}]]]

       [:li.inactive
        [:a
         {:color (:dark colors)}]]

       [:li.active
        [:a
         {:color (:blue colors)}]]]

      [:footer
       {:margin-top "25px"
        :margin-bottom "25px"}

       [:.content-container
        {:border-top-color (:border colors)
         :border-top-style "dotted"
         :border-top-width "3px"
         :padding-top "15px"}
        (on-mobile
          {:padding-left (:mobile-margin sizes)
           :padding-right (:mobile-margin sizes)
           :box-sizing "border-box"
           :text-align "center"})]

       [:p
        {:margin "0px"
         :font-size "12px"
         :line-height "1.5em"}]
       [:.webrings
        {:margin-top "10px"
         :display "flex"}
        (on-mobile
          {:justify-content "center"})

        [:a
         {:opacity 0.6}
         [:&:hover
          {:opacity 1}]]]]

      [:.content-container
       {:width (:content-width sizes)
        :margin "0px auto"}

       (on-mobile
         {:width "100vw"})

       [:&.text-content
        {:background-color (:white colors)
         :padding "20px"
         :box-sizing "border-box"
         :margin "15px auto"}
        (on-mobile
          {:padding (:mobile-margin sizes)})]

       [:&.page-intro
        {:margin-bottom "15px"}]
       
       [:&.photo-gallery
        [:p
         {:line-height "1.5em"}
         (on-mobile
           {:margin-left (:mobile-margin sizes)
            :margin-right (:mobile-margin sizes)})]
        
        [:.page--title
         (on-mobile
           {:margin-left (:mobile-margin sizes)
            :margin-right (:mobile-margin sizes)})]
        
        [:.page--meta
         (on-mobile
           {:margin-left (:mobile-margin sizes)
            :margin-right (:mobile-margin sizes)})]]]

      [:.page--title
       {:font-size "32px"}]

      [:.page--cover-image
       {:width "75%"
        :display "block"
        :margin-top "15px"}
       (on-mobile
         {:width "100%"})]

      [:.page--meta
       {:font-size "16px"
        :color (:alt-text colors)
        :margin-top "5px"}]
      [(sel/+ :.page--meta :.gallery--items)
       {:margin-top "15px"}]

      [:.page--footer
       {:font-size "14px"
        :color (:alt-text colors)
        :margin-top "15px"}]

      [:.page--md-content
       {:font-size "18px"}
       (on-mobile
         {:font-size "16px"})

       [:h1
        {:font-weight "normal"}]

       [:h2
        {:font-weight "normal"
         :font-size "28px"}]

       [:h3
        {:font-weight "normal"
         :font-size "24px"
         :margin "1em 0px .3em 0px"}]

       (map #(vector (sel/+ % :p) {:margin-top "0px"}) [:h1 :h2 :h3])

       [:p
        {:line-height "1.7em"}
        [:& (sel/nth-last-child "1")
         {:margin-bottom "0px"}]]

       [:a
        {:text-decoration "underline"}
        [:&:hover
         {:text-decoration "none"}]]

       [:blockquote
        {:border-left-width "5px"
         :border-left-color (:off-white colors)
         :border-left-style "solid"
         :padding "10px 0px 10px 15px"
         :margin-left "10px"}

        [:p (sel/nth-child "1")
         {:margin-top "0px"}]
        [:p (sel/nth-last-child "1")
         {:margin-bottom "0px"}]]

       [:pre
        {:background-color (:dark colors)
         :color (:off-white colors)
         :font-size "14px"
         :padding "10px"
         :max-width "100%"
         :overflow-x "auto"}]

       [:table
        {:width "100%"
         :margin-top "15px"
         :background (:off-white colors)
         :border-spacing "0px 0px"
         :border-width "1px"
         :border-color (:border colors)
         :border-style "solid"}
        font-mono

        [:th
         {:font-weight "bold"
          :text-align "left"
          :padding "5px"
          :border-bottom-width "1px"
          :border-bottom-color (:border colors)
          :border-bottom-style "solid"}]

        [:tbody
         [:tr
          [(sel/& (sel/nth-child "2n+1"))
           {:background-color (color/lighten (:off-white colors) 3)}]
          [:td
           {:padding "5px"}]]]]

       ; Preview images that come right after the post date need a bit of
       ; padding baked in
       [:img [:& (sel/nth-child "1")
        {:margin-top "15px"}]]

       [:img
        {:max-width "100%"}]

       [:.image-caption
        {:text-align "center"
         :font-style "italic"
         :margin-top "15px"
         :color (:alt-text colors)}

        [:a
         {:color (:alt-text colors)}]]

       [:ol :ul
        (on-mobile
          {:padding-left "15px"})]

       [:li
        {:line-height "1.7em"
         :margin-bottom "10px"}
        [:& (sel/nth-last-child "1")
         {:margin-bottom "0px"}]]

       [:.footnotes
        {:font-size "14px"}]]

      ;;;;
      ;; Gallery index
      ;;
      [:.gallery--items
       {:display "grid"
        :grid-template-columns "repeat(2, 50fr)"
        :grid-gap "5px"}
       (on-mobile
         {:grid-template-columns "100fr"
          :grid-gap "10px"})]

      [:.gallery--item
       {:display "block"
        :background "#000"
        :background-size "cover"
        :background-position "center center"
        :background-repeat "no-repeat"
        :aspect-ratio 1.25
        :position "relative"
        :grid-column "span 1"}

       [:&:hover
        [:.gallery--item-meta
         {:background (:blue colors)
          :cursor "pointer"}]]

       [:.gallery--item-meta
        {:position "absolute"
         :bottom 0
         :left 0
         :right 0
         :background "rgba(0, 0, 0, 0.85)"
         :color (:white colors)
         :padding "10px 15px"
         :text-align "center"}

        [:.gallery--item-name
         {:font-size "24px"}]
        [:.gallery--item-desc
         {:font-style "italic"
          :font-size "14px"
          :margin-top "3px"}]]]

      ;;;;
      ;; Dated list
      ;;
      [:ul.dated-list
       {:padding "0px"
        :list-style "none"
        :margin "0px"}

       [:li 
        {:padding "3px 0px"
         :line-height "1.3em"}]
       [:.dated-list--date
        font-mono
        {:font-size "14px"
         :margin-right "5px"}
        (on-mobile
          {:display "block"})]]

      inline-sheets
      (render-inventory site))))

(defn route []
  {:route/type :file
   :route/emit-path "static/style.css"
   :file/render render-main})
