(ns vesto.routes.static)

(defn route []
  {:route/type :static-dir
   :static-dir/src-path "resources/static"
   :route/emit-path "static"})
