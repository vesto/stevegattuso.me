(ns vesto.routes.blog-archive
  (:require [hiccup.core :refer [html]]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [vesto.routes.stylesheets :as style]
            [vesto.layouts.base]))

(defn render [p s]
  (let [all-posts (->> s
                       (filter #(= (:collection/identifier %) "blog"))
                       (sort
                         #(compare (tpl/route-md %2 :date)
                                   (tpl/route-md %1 :date))))]
    (html
      [:div.content-container.text-content
       [:h1 "Blog"]
       [:p "I generally like writing about cities, technology, digital privacy, and any interesting links I find around the web. If you like what you see here, consider subscribing to my " [:a {:href (tpl/url-for s "feed.xml")} "RSS feed"] " or by following me on Mastodon (see link at the top)."]
       [:p "Posts marked with a " [:span.blog-index--star "&starf;"] " are my more notable entries, 🔗 are external links, and 🪧 are more longform posts."]

       [:ul.dated-list
        (map
          (fn [post]
            (let [date (tpl/route-md post :date)
                  title (tpl/route-md post :title)
                  link (:route/emit-path post)]
              [:li
               [:span.dated-list--date (tpl/format-date "yyyy-MM-dd:" date)]
               (cond
                 (some #{"best-of"} (into [] (tpl/route-md post :categories)))
                 [:span.blog-index--star "&starf; "]

                 (tpl/route-md post :link)
                 [:span "🔗 "]

                 :else
                 [:span "🪧 "])
               [:a {:href (tpl/url-for s link)} title]]))

          all-posts)]])))

(defn render-stylesheet [s]
  [[:.blog-index--star
    {:color (style/colors :yellow)}]])

(defn route []
  {:route/type :page
   :route/emit-path "blog/index.html"
   :route/metadata {:title "Blog"
                    :nav ["Blog"]
                    :stylesheet/render render-stylesheet}
   :page/render render
   :page/layout vesto.layouts.base/render})
