(ns vesto.routes.wiki-links
  (:require [clj-yaml.core :as yaml]
            [hiccup.core :refer [html]]
            [markdown.core :as md]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [vesto.utils :as utils]
            [vesto.layouts.page]))

(defn render-link [site link]
  [:div.wiki-links--link
   [:a.wiki-links--title
    {:href (:url link)}
    [:b (:title link)]]
   (when (:comments link)
     (:html (utils/parse-md site (:comments link))))])

(defn render-category [site links category]
  (let [clinks (filter #(= (:category %) (:id category)) links)]
    [:div.page--md-content
     [:h3 (:id category)]
     [:p [:i (:desc category)]]
     (map #(render-link site %) clinks)]))

(defn render [p s]
  (let [data (->> (slurp "resources/data/links.yaml")
                   (yaml/parse-string))]
    (html
      [:p "Documenting the nooks and crannies of the internet that I've found particularly interesting, weird, or worthwhile for some reason or another."]
      (map
        #(render-category s (:links data) %)
        (:categories data)))))

(defn render-stylesheet [s]
  [[:.wiki-links--title
    {:font-size "18px"}]

   [:.wiki-links--link
    {:margin-top "20px"}
    [:p
     {:font-size "16px"
      :margin-top "5px"}]]])

(defn route []
  {:route/type :page
   :route/emit-path "wiki/links.html"
   :route/metadata {:title "Links"
                    :nav ["Wiki" "Links"]
                    :created-at "2021-12-28"
                    :updated-at "2022-06-09"
                    :wiki-id "links"
                    :stylesheet/render render-stylesheet}
   :page/render render
   :page/layout vesto.layouts.page/render})
