(ns vesto.routes.md-pages
  (:require [hiccup.core :refer [html]]
            [markdown.core :as md]
            [clojure.string :as string]
            [babashka.fs :as fs]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [vesto.utils :as utils]
            [vesto.layouts.page]))

(defn render-emit-path [p]
  (let [wiki-id (tpl/route-md p :wiki-id)
        emit-path (tpl/route-md p :emit-path)]
    (if (some? wiki-id)
      (format "wiki/%s.html" wiki-id)
      emit-path)))

(defn render-page [page site]
  (:html (utils/parse-md site (slurp (str (:collection/item/path page))))))

(defn route []
  {:route/type :collection
   :collection/identifier "md-pages"
   :collection/item-generator (tpl/generate-md-items "resources/pages")
   :collection/directory "resources/pages"
   :collection/layout vesto.layouts.page/render
   :collection/render-metadata tpl/fetch-md-frontmatter
   :collection/render-emit-path render-emit-path
   :collection/render-item render-page})
