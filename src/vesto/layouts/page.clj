(ns vesto.layouts.page
  (:require [hiccup.core :refer [html]]
            [clojure.string :as string]
            [jupi.tpl-helpers :as tpl :refer [defn]]
            [babashka.fs :as fs]
            [vesto.layouts.base :as base]))

(defn parse-date [d]
  (if (string? d)
    (.parse (java.text.SimpleDateFormat. "yyyy-MM-dd") d)
    d))

(defn render [page-content p s]
  (let
    ; Determine the source file's last created/modified dates
    [created-at (parse-date (tpl/route-md p :created-at))
     updated-at (parse-date (tpl/route-md p :updated-at))

     fmt "MMMM d, Y"
     fmt-created (tpl/format-date fmt created-at)
     fmt-modified (tpl/format-date fmt updated-at)

     title (if (some? (tpl/route-md p :nav))
             (string/join " &raquo " (tpl/route-md p :nav))
             (tpl/route-md p :title))

     page-wrapper
     (html
       [:div.content-container.text-content
        [:div.page--title title]
        (when (tpl/route-md p :cover-image)
          [:img.page--cover-image {:src (tpl/route-md p :cover-image)}])
        [:div.page--md-content page-content]
        (if (some? fmt-created)
          [:div.page--footer
           (str "Page created on " fmt-created ". ")
           (if (and (some? fmt-modified)
                    (not= fmt-created fmt-modified))
             (str "Last updated " fmt-modified))])])]

    (base/render page-wrapper p s)))
