(ns vesto.layouts.base
  (:require [hiccup.core :refer [html]]
            [jupi.tpl-helpers :as tpl :refer [defn]]))

(def current-year (.get (java.util.Calendar/getInstance) java.util.Calendar/YEAR))

(def nav
  [{:title "Home"
    :path ["Home"]
    :link "/"}
   {:title "Blog"
    :path ["Blog"]
    :link "/blog"}
   {:title "Wiki"
    :path ["Wiki"]
    :link "/wiki/wiki-index.html"}
   {:title "Photos"
    :path ["Photos"]
    :link "/photos"}
   {:title "Boombox"
    :path ["Boombox"]
    :link "/boombox"}
   {:title "About"
    :path ["About"]
    :link "/wiki/about.html"}])

(defn generate-nav [site current-page]
  (let [cnav (tpl/route-md current-page :nav)
        selected (or cnav [])]
    [:div.primary-nav
     [:ul.nav
      (map (fn [item]
             [:li {:class (if (= (first cnav) (first (:path item)))
                            :active
                            :inactive)}
              [:a {:href (:link item)} (last (:path item))]
              [:span.divider " // "]])
           nav)]]))

(defn og [property content]
  [:meta {:property property :content content}])

(defn render [page-content p s]
  (let [title (str (tpl/route-md p :title) " // steve gattuso")
        summary (tpl/route-md p :summary)
        preview-img (tpl/route-md p :preview-image)
        preview-img (if preview-img
                      (tpl/url-for s preview-img)
                      nil)]
    (html
      [:html
       [:head
        [:title title]
        [:meta {:http-equiv "content-type"
                :content "text/html; charset=utf-8"}]
        [:meta {:name "viewport" :content "width=device-width, initial-scale=1;"}]
        [:link {:rel "stylesheet" :href (tpl/url-for s "static/style.css")}]
        [:link {:rel "icon" :href (tpl/url-for s "static/favicon.png")}]

        (when-not (tpl/dev-server? s)
          [:script {:data-goatcounter "https://stevegattuso.goatcounter.com/count"
                    :async "yes"
                    :src "//gc.zgo.at/count.js"}])

        ;; Open graph metadata
        (og "og:site_name" "stevegattuso.me")
        (og "og:locale" "en_US")
        (og "og:title" title)
        (og "og:url" (tpl/url-for s p))
        (cond
          (contains? #{"blog"} (:collection/identifier p))
          (list (og "og:type" "article")
                (og "og:article:published_time"
                    (tpl/format-date "yyyy-MM-dd" (tpl/route-md p :date)))
                (og "og:article:author" "Steve Gattuso"))
          
          :else
          (og "og:type" "website"))
        (when preview-img
          [:meta {:property "og:image" :content preview-img}])
        (when summary
          [:meta {:property "og:description" :content summary}])

        (tpl/inject-autoreload s)]
       [:body
        [:header
         [:div.content-container.title-container
          [:a.site-title {:href (tpl/url-for s "")}
           [:span "steve"]
           "gattuso"]
          [:div.top-links
           [:ul
            [:li [:a {:target "_blank" :href "https://hackny.social/@steve"} "Mastodon"]]
            [:li [:a {:target "_blank" :href "https://git.sr.ht/~vesto"} "SourceHut"]]
            [:li [:a {:href "mailto:steve@stevegattuso.me"} "Email"]]]]]
         [:div.content-container.meta
          [:div.photo]
          [:div.bio
           [:p "
            I'm a programmer interested in urbanism 🏙️, sailing ⛵, traveling 🚄, and creating a more sustainable economy 🍃. This website is an always-in-progress repository for documenting my latest ideas and projects."]]]]

        [:div.stripe (repeat 6 [:div])]

        (generate-nav s p)

        page-content

        [:footer
         [:div.content-container
          [:p "&copy; " current-year " Steve Gattuso. All content licensed under "
           [:a
            {:href "https://creativecommons.org/licenses/by-sa/4.0/"
             :target "_blank"}
            "CC BY-SA 4.0"] "."]
          [:div.webrings
           [:a {:href "https://webring.recurse.com"}
            [:img {:src "https://webring.recurse.com/icon.png"
                   :width 32
                   :height 32}]]]]]]])))
