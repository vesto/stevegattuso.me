(ns vesto.main
  (:require [jupi.core :as jupi]
            [jupi.tpl-helpers :as tpl]
            [markdown.core :as md]
            [clojure.string :as string]))

(defn -main []
  (jupi/compile-site! "vesto.routes" {:site-url "https://www.stevegattuso.me/"}))

(comment
  (require '[clojure.pprint :refer [pprint]])
  (jupi/compile-site! "vesto.routes" {:debug true
                                      :site-url "http://localhost:8000/"})
  (pst)

  (def server (jupi/start-dev-server!))
  (.stop server)
)
