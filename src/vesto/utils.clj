(ns vesto.utils
  (:require [clojure.string :as string]
            [markdown.core :as md]
            [jupi.tpl-helpers :as tpl :refer [defn]]))

(def wiki-regex #"\[\[([A-z0-9\-]{1,})\]\]")

(defn id->page [s id]
  (first (filter #(or (= (tpl/route-md % :wiki-id) id)
                      (= (tpl/route-md % :slug) id)) s)))

(defn wiki-link [site id]
  (let [page (id->page site id)]
    (if (some? page)
      (format "<a href=\"%s\" class=\"wiki-link\">%s</a>"
              (tpl/url-for site page)
              (tpl/route-md page :title))
      (do
        (println "Bad link for ID:" id)
        "{{BAD LINK}}"))))


(defn wiki-url [s id]
  (let [page (id->page s id)]
    (if (some? page)
      (tpl/url-for s page)
      (do
        (println "Bad link for ID:" id)
        "{{BAD LINK}}"))))

(defn wiki-link-parser
  "Custom parser for [[wiki links]]"
  [site text state]
  [(-> text
       (string/replace wiki-regex #(wiki-link site (second %))))
   state])


(defn parse-md [site text]
  (md/md-to-html-string-with-meta
    text
    :footnotes? true
    :custom-transformers [(partial wiki-link-parser site)]))
