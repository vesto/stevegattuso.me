(ns jupi.tpl-helpers
  (:require [clojure.string :as string]
            [babashka.fs :as fs]
            [markdown.core :as md]))

(defmacro defn
  "my static site generator, my rules."
  [fn-name params & body]
  `(do
     ~(concat `(clojure.core/defn ~fn-name ~params) body)
     (jupi.core/compile-site!)))

(clojure.core/defn get-data
  "Finds all data routes and converts them into a handy map"
  [site]
  (as-> site x
       (filter #(= (:route/type %) :data) x)
       (into {} x)
       (:route/metadata x)))

(clojure.core/defn site-md
  "Fetches a key out of the site's aggregated :data routes"
  [site k]
  (k (get-data site)))

(clojure.core/defn route-md
  "Fetches a key out of a route's metadata"
  [md md-key]
  (get-in md [:route/metadata md-key]))

(clojure.core/defn dev-server? [site] (boolean (site-md site :debug)))

(clojure.core/defn inject-autoreload
  "Injects a small javascript utility for auto-reloading"
  [site]
  (if (dev-server? site)
    (str "<script>" (slurp "resources/auto-reload.js") "</script>")
    nil))

(defmulti format-date (fn [fmt date] (type date)))
(defmethod format-date java.util.Date [fmt date]
  (.format (java.text.SimpleDateFormat. fmt) date))
(defmethod format-date java.nio.file.attribute.FileTime [fmt ft]
  (let [date (java.util.Date. (.toMillis ft))]
    (.format (java.text.SimpleDateFormat. fmt) date)))
(defmethod format-date nil [fmt date] nil)

(defmulti url-for (fn [site to] (cond
                                  (map? to) :route
                                  (string? to) :str
                                  :else nil)))
(defmethod url-for :str [site path]
  "Fetches an absolute URL to a path. Useful for creating href values referencing
  files/pages on your site."
  [site path]
  (let [normalized (if (= \/ (first path))
                     (string/join (drop 1 path))
                     path)]
    (str
      (or (site-md site :site-url) "/")
      normalized)))
(defmethod url-for :route [s p]
  (let [emit-path (:route/emit-path p)
        parts (string/split emit-path #"/")
        file (last parts)
        path (if (= (last parts) "index.html")
               (string/join "/" (drop-last 1 parts))
               emit-path)]
    (str (site-md s :site-url) path)))

(clojure.core/defn fetch-md-frontmatter [path]
  (->> path
       (str)
       (slurp)
       (md/md-to-html-string-with-meta)
       (:metadata)))

(clojure.core/defn generate-md-items
  "Given a path, this will generate a function that can be passed as :collection/item-generator which returns vector of routes, one for each markdown file found."
  [path]
  (fn []
    (->> path
         (fs/list-dir)
         (filter #(and (fs/readable? %) (= "md" (fs/extension %)))))))
