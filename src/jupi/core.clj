(ns jupi.core
  (:require [clojure.spec.alpha :as s]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.file :as ring-file]
            [ring.middleware.content-type :as ring-content-type]
            [markdown.core :as md]
            [bultitude.core :as b]
            [babashka.fs :as fs]))

(defn get-cache-path
  "Returns the base cache path or, optionally, the full joined path of the argument"
  ([]
   (->
     (io/file (System/getProperty "user.dir") "resources" "_cache")
     (.getPath)))
  ([path]
   (-> (get-cache-path)
       (io/file path)
       (.getPath))))

(defn get-output-path
  "Returns the base output path or, optionally the full joined path of the argument."
  ([]
   (->
     (io/file (System/getProperty "user.dir") "resources" "_output")
     (.getPath)))
  
  ([subpath]
   (-> (get-output-path)
       (io/file subpath)
       (.getPath))))


(defn emit-file!
  "Writes a file to the website directory"
  [path content]
  (let [full-path (get-output-path path)]
    (io/make-parents full-path)
    (spit full-path content)))

(defn resolve-route [sym]
  (apply (var-get (resolve (symbol (str sym) "route"))) []))

(defn fetch-route-defns [prefix]
  (let [namespaces (->> (b/namespaces-on-classpath)
                        (filter #(string/starts-with? (str %) prefix)))
        ; This is a potentially hideous thing to do but it's ok because I want
        ; to do it and I left a note.
        _ (doall (map require namespaces))
        routes (map resolve-route namespaces)]
    routes))

;;;;
;; Extract metadata for route
;;
(defmulti generate-route-data (fn [route-defn] (:route/type route-defn)))
(defmethod generate-route-data :default
  [route-defn]
  route-defn)

(defn generate-collection-route
  "Given the path to an item, render its metadata and generate a route definition for use in the render step."
  [route-defn path]
  (let [metadata (apply
                   (:collection/render-metadata route-defn)
                   [path])
        partial-route {:route/type :collection
                       :route/metadata metadata
                       :collection/identifier (:collection/identifier route-defn)
                       :collection/item/render (:collection/render-item route-defn)
                       :collection/item/layout (:collection/layout route-defn)
                       :collection/item/path path}
        emit-path (apply
                    (:collection/render-emit-path route-defn)
                    [partial-route])]
    (merge partial-route {:route/emit-path emit-path})))

(defmethod generate-route-data :collection
  [route-defn]
  (->> (apply (:collection/item-generator route-defn) [])
       (map #(generate-collection-route route-defn %))))

(defn compile-routes
  "Inputs a route definition and outputs a sequence of all files that will be
  rendered + their metadata"
  [routes]
  (->> routes
       (map generate-route-data)
       (reduce (fn [v map-or-seq]
                 (if (map? map-or-seq)
                   (conj v map-or-seq)
                   (into [] (concat v map-or-seq)))) [])))

;;;;
;; Render routes
;;
(defmulti render-route! (fn [page site] (:route/type page)))
(defmethod render-route! :page
  [page site]
  (let [page-content (apply (:page/render page) [page site])
        output-content (if (contains? page :page/layout)
                         (apply (:page/layout page) [page-content page site])
                         page-content)]
    (emit-file! (:route/emit-path page) output-content)))

(defmethod render-route! :file
  [file-metadata site-metadata]
  (let [file-content (apply (:file/render file-metadata) [site-metadata])]
    (emit-file! (:route/emit-path file-metadata) file-content)))

(defmethod render-route! :static-dir
  [dir-metadata site-metadata]
  (fs/copy-tree
    (:static-dir/src-path dir-metadata)
    (get-output-path (:route/emit-path dir-metadata))))

(defmethod render-route! :collection
  [item-data site-data]
  (let [item-content (apply (:collection/item/render item-data) [item-data site-data])
        output-content (if (contains? item-data :collection/item/layout)
                         (apply (:collection/item/layout item-data) [item-content item-data site-data])
                         item-content)]
    (emit-file! (:route/emit-path item-data) output-content)))

(defmethod render-route! :data
  [col-meta site-meta]
  :default)

;;;;
;; Other helpers for the compilation pipeline
;;
(defn clear-site! []
  ; Create the directory once just in case it doesn't already exist (which
  ; would cause the delete-directory! call to fail)
  (fs/walk-file-tree
    (get-output-path)
    {:visit-file (fn [path attrs] (fs/delete path) :continue)
     :post-visit-dir (fn [dir attrs]
                       (when-not (= (str dir) (get-output-path))
                         (fs/delete dir))
                       :continue)}))

(defonce update-timestamp (atom (java.util.Date.)))
(defonce compile-params (atom nil))
(defonce compiling (atom false))

(defn trigger-update!
  "Updates the update-timestamp with the latest timestamp. This should
  trigger any polling browsers to refresh"
  []
  (swap! update-timestamp (fn [cur] (java.util.Date.))))

(defn compile-site!
  ([]
   (let [prev-params @compile-params
         now-ts (.getTime (java.util.Date.))
         then-ts (.getTime @update-timestamp)
         duration (- now-ts then-ts)]
     (cond
       (deref compiling)
       "Compiler already running..."

       (nil? @compile-params)
       "No compile params set. Skipping compile!"

       (> duration 500)
       (do
         (apply compile-site! @compile-params)
         "Compiled!")

       :else
       (str "Last compile was " (double (/ duration 1000)) "s ago. Skipping compile!"))))


  ([ns-prefix]
   (compile-site! ns-prefix {}))

  ([ns-prefix inject-metadata]
   (swap! compiling (fn [prev] true))
   (swap! compile-params (fn [prev] [ns-prefix inject-metadata]))

   (try
     (let [routes (fetch-route-defns ns-prefix)
           alt-routes (if (> (count inject-metadata) 0)
                        (conj routes {:route/type :data :route/metadata inject-metadata})
                        routes)
           site-data (compile-routes alt-routes)]
       (println "CLEAR: " (get-output-path))
       (clear-site!)

       (println "RENDERING...")
       (doall (map #(render-route! % site-data) site-data))
       (trigger-update!)

       (println "DONE!")
       (swap! compiling (fn [prev] false)))
     (catch Exception e
       (swap! compiling (fn [p] false))
       (throw e)))))

;;;;
;; Dev server
;;

(defn not-found-handler [req]
  (if (= (:uri req) "/_internal/update-timestamp")
    {:status 200
     :headers {"Content-Type" "textplain"}
     :body (str (.getTime @update-timestamp))}
    {:status 404
     :headers {"Content-Type" "text/html"}
     :body "Not found"}))

(defn start-dev-server! []
  (jetty/run-jetty
    (ring-file/wrap-file not-found-handler (get-output-path))
    {:port 8000
     :join? false}))

(comment
  (require '[clojure.tools.trace :as trace])
  (require '[clojure.pprint :as pprint])

  (clear-site!)
)
